# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Temporary placeholder for index, just to have something for the presentation.
    url(r'^$', 'apps.bw_bookworm.views.index', name = 'index'),
    url(r'^search/', 'apps.bw_bookworm.views.search_view.search', name = 'search'),
    # /

    url(r'^accounts/', include('apps.bw_user.urls')),
    url(r'^messages/', include('postman.urls')),
	url(r'^admin/', include(admin.site.urls)),
    url(r'^books/', include('apps.bw_books.urls')),
    url(r'^reports/', include('apps.bw_bookworm.urls')),
    url(r'^trans/', include('apps.bw_trans.urls')),

) + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
