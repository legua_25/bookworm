# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_noop as _
import os
"""
Django settings for Bookworm project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
SECRET_KEY = '1q1)ymed7@-lvc$b%opj#gc7+&e49!4*yzir+$v)!d_lstxz*('

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [ 'localhost', '127.0.0.1' ]

# Applications
INSTALLED_APPS = (

	'django_admin_bootstrapped.bootstrap3',
	'django_admin_bootstrapped',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',

	'apps.bw_user',
    'apps.bw_books',
    'apps.bw_trans',

	'allauth',
	'allauth.account',
    'allauth.socialaccount',
    # 'allauth.socialaccount.providers.facebook',
    # 'allauth.socialaccount.providers.twitter',

    'django_extensions',
    'model_utils',
    'djcelery',
    'imagekit',
    'djsupervisor',
    'gunicorn',
    'widget_tweaks',
    'currencies',
    'postman',

)
SITE_ID = 1
# Middleware
MIDDLEWARE_CLASSES = (

    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

)
ROOT_URLCONF = 'bookworm.urls'
WSGI_APPLICATION = 'bookworm.wsgi.application'


# Database
DATABASES = {

	# TODO: Meme, ponle aqui tus datos. Haz referencia a este archivo como el modulo de settings de Django (Settings > Django)
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'bwdb.sqlite3'),
    }

}

# Internationalization
LANGUAGE_CODE = 'en-us'
LANGUAGES = (
	('en', _('English')),
	('es', _('Spanish'))
)

TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATICFILES_DIRS = (
	os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'apps/bw_user/static'),
    os.path.join(BASE_DIR, 'apps/bw_bookworm/static'),
)
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# Templates
TEMPLATE_CONTEXT_PROCESSORS = (

	'django.core.context_processors.request',
	'django.contrib.auth.context_processors.auth',
	'allauth.account.context_processors.account',
	'allauth.socialaccount.context_processors.socialaccount',
	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',
	'django.core.context_processors.static',
	'django.contrib.messages.context_processors.messages',
	'currencies.context_processors.currencies',
	'postman.context_processors.inbox'

)
TEMPLATE_DIRS = (
	os.path.join(BASE_DIR, 'templates'),
    os.path.join(BASE_DIR, 'apps/bw_bookworm/templates'),
    os.path.join(BASE_DIR, 'apps/bw_user/templates'),
)

# Authentication
AUTHENTICATION_BACKENDS = (
    "allauth.account.auth_backends.AuthenticationBackend",
)
LOGIN_REDIRECT_URL = '/accounts/'

OPENEXCHANGERATES_APP_ID = 'f4ea5ed449b442f0a06b165e7e55c318'
AUTH_USER_MODEL = 'bw_user.User'
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_MIN_LENGTH = 5
ACCOUNT_PASSWORD_MIN_LENGTH = 8
ACCOUNT_USER_MODEL_USERNAME_FIELD = 'username'
ACCOUNT_USER_MODEL_EMAIL_FIELD = 'email'
ACCOUNT_SIGNUP_PASSWORD_VERIFICATION = False
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = False
ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = '/accounts/login/'
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_LOGOUT_ON_GET = True
SOCIALACCOUNT_AUTO_SIGNUP = False

# Email backends
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DEBUG = True
