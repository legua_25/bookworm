from django import forms
from django.utils.translation import ugettext_lazy as _
from ..models import Purchase, Transaction
from currencies.models import Currency

__author__ = 'Guilherme'

class PurchaseBookForm(forms.ModelForm):

	class Meta:
		model = Purchase
		fields = ('currency', 'price')

	price = forms.DecimalField(
		decimal_places = 2,
		max_digits = 8,
		required = False,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Price') })
	)

class TransactionRequestForm(forms.ModelForm):

	class Meta:
		model = Transaction
