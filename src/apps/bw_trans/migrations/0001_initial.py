# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Operation',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='unique transaction ID', db_index=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
                ('status', models.PositiveSmallIntegerField(default=0, choices=[('Unset', 0), ('Accepted', 1), ('Confirmed', 2)])),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'transacts',
                'verbose_name': 'transaction',
                'verbose_name_plural': 'transactions',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Giveaway',
            fields=[
                ('operation_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='bw_trans.Operation')),
            ],
            options={
                'db_table': 'freebie_transacts',
                'verbose_name': 'giveaway transaction',
                'verbose_name_plural': 'giveaway transactions',
            },
            bases=('bw_trans.operation',),
        ),
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('operation_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='bw_trans.Operation')),
                ('price', models.DecimalField(verbose_name='price', max_digits=8, decimal_places=2)),
            ],
            options={
                'db_table': 'purchase_transacts',
                'verbose_name': 'purchase transaction',
                'verbose_name_plural': 'purchase transactions',
            },
            bases=('bw_trans.operation',),
        ),
        migrations.CreateModel(
            name='Trade',
            fields=[
                ('operation_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='bw_trans.Operation')),
            ],
            options={
                'db_table': 'trade_transacts',
                'verbose_name': 'trade transaction',
                'verbose_name_plural': 'trade transactions',
            },
            bases=('bw_trans.operation',),
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='unique transaction ID', db_index=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
                ('accept_date', models.DateTimeField(default=None, null=True, verbose_name='acceptance date', blank=True)),
                ('confirm_date', models.DateTimeField(default=None, null=True, verbose_name='confirmation date', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='active')),
                ('operation', models.ForeignKey(related_name='transacts', default=None, verbose_name='transaction', to='bw_trans.Operation', null=True)),
            ],
            options={
                'db_table': 'transact_requests',
                'verbose_name': 'transaction request',
                'verbose_name_plural': 'transactions requests',
            },
            bases=(models.Model,),
        ),
    ]
