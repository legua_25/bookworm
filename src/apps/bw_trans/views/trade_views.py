# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils.translation import ugettext_lazy as _
from django.utils.decorators import method_decorator
from apps.bw_books.models import *
from ..forms import *
from django.shortcuts import render_to_response, RequestContext
from ..models import *
from uuid import UUID
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from postman.api import pm_write, pm_broadcast
from django.template.loader import render_to_string
import datetime

__all__ = [ 'trans_trade_execute_view', 'trans_trade_view', 'trans_trade_resolve_view' ]
__author__ = 'Guilherme'

class TransTradeOfferView(View):

    @method_decorator(login_required)
    def post(self, request, copy_id = None, *args, **kwargs):

        try:
            copy = PhysicalBookCopy.objects.get(uid = str(UUID(copy_id)))
        except PhysicalBookCopy.DoesNotExist:
            return HttpResponseBadRequest()
        else:
            if str(UUID(copy.trade_id)) == '':
                return HttpResponseBadRequest()

            book_copies = PhysicalBookCopy.objects.filter(owner = request.user, active = True)
            trans = Transaction(
                owner = copy.owner,
                user = request.user,
                operation = Operation.objects.get(uid = str(UUID(copy.trade_id)))
            )

            return render_to_response('trans_templates/trade_offer.html', context_instance = RequestContext(request, locals()))
class TransTradeExecuteView(View):

    @method_decorator(login_required)
    def post(self, request, copy_id = None, *args, **kwargs):

        offer_id = request.POST['offer-id']

        try:
            copy = BookCopy.objects.get_subclass(uid = str(UUID(copy_id)))
            offer = BookCopy.objects.get_subclass(uid = str(UUID(offer_id)))
        except BookCopy.DoesNotExist:
            return HttpResponseBadRequest()
        else:
            if copy.for_trade is True and offer.for_trade is True:

                op = Operation.objects.get_subclass(uid = str(UUID(copy.trade_id)))
                if not Transaction.objects.active().filter(owner = copy.owner, user = request.user, operation = op).exists():

                    trans = Transaction(
                        owner = copy.owner,
                        user = request.user,
                        operation = op
                    )
                    trans.save()

                    pm_write(request.user, copy.owner, _('Trade offer for your book'), render_to_string('trans_templates/msg_offer.txt', context_instance = RequestContext(request, locals())))

                    # TODO: Add something here

                return redirect(reverse_lazy('details', args = [ copy.book.uid_str, copy.uid_str ]))

class TransTradeResolutionView(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        offer_id = request.GET['offer-id']
        copy_id = request.GET['copy-id']
        trans_id = request.GET['trans-id']
        ans = request.GET.get('answer', 'no')

        try:
            trans = Transaction.objects.active().get(uid = str(UUID(trans_id)))
            copy = BookCopy.objects.active().get_subclass(uid = str(UUID(copy_id)))
            offer = BookCopy.objects.active().get_subclass(uid = str(UUID(offer_id)))

            if ans == 'yes':
                others = Transaction.objects.filter(owner = trans.owner).exclude(uid = trans_id)
                users = [t.user for t in others]
                pm_broadcast(trans.user,
                             users,
                             _('Your offer has been declined.'),
                             render_to_string('trans_templates/msg_decline.txt',
                                              context_instance = RequestContext(request, locals())))
                pm_write(trans.user,
                         request.user,
                         _('Your offer has been accepted!'),
                         render_to_string('trans_templates/msg_accept.txt',
                                          context_instance = RequestContext(request, locals())))
                others.update(active = False)

                trans.accept_date = datetime.datetime.now()
                trans.save()
            else:
                pm_write(trans.user, request.user, _('Your offer has been declined.'), render_to_string('trans_templates/msg_decline.txt',
                                          context_instance = RequestContext(request, locals())))
                trans.active = False
                trans.save()

            return redirect(reverse_lazy('details', args = [ copy.book.uid_str, copy.uid_str ]))
        except Transaction.DoesNotExist, BookCopy.DoesNotExist: return HttpResponseBadRequest()

trans_trade_view = TransTradeOfferView.as_view()
trans_trade_execute_view = TransTradeExecuteView.as_view()
trans_trade_resolve_view = TransTradeResolutionView.as_view()