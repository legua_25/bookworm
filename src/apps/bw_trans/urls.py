# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import url, patterns, include

urlpatterns = patterns('apps.bw_trans.views',
    url(r'^resolve/$', 'trans_trade_resolve_view', name = 'resolve_trade'),
    url(r'^trade/(?P<copy_id>[0-9a-f]{32})/', include(patterns('apps.bw_trans.views',

        url(r'^$', 'trans_trade_view', name = 'trade'),
        url(r'^execute/$', 'trans_trade_execute_view', name = 'execute_trade'),
    )))
)