# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField
from django.db.models import Model, Manager
from django.conf import settings
from django.db import models
from uuid import UUID

__all__ = [ 'Transaction']

User = settings.AUTH_USER_MODEL
Operation = 'bw_trans.Operation'

class TransactionManager(Manager):

	def active(self): return self.get_queryset().filter(active = True)
	def accepted_for(self, transact): return self.get_queryset().filter(active = True, transact = transact, accept_date__isnull = False)
	def confirmed_for(self, transact): return self.get_queryset().filter(active = True, transact = transact, confirm_date__isnull = False)

class Transaction(Model):

	class Meta(object):

		verbose_name = _('transaction request')
		verbose_name_plural = _('transactions requests')
		app_label = 'bw_trans'
		db_table = 'transact_requests'

	uid = UUIDField(
		primary_key = True,
	    db_index = True,
	    auto = True,
	    editable = False,
	    verbose_name = _('unique transaction ID')
	)
	owner = models.ForeignKey(
		User,
	    related_name = 'transacts',
	    verbose_name = _('owner')
	)
	user = models.ForeignKey(
		User,
	    related_name = 'offerings',
	    verbose_name = _('offering user')
	)
	operation = models.ForeignKey(
		Operation,
	    related_name = 'transacts',
        null = True,
        default = None,
	    verbose_name = _('transaction')
	)
	create = models.DateTimeField(
		auto_now_add = True,
	    auto_now = False,
	    editable = False,
	    verbose_name = _('creation date')
	)
	accept_date = models.DateTimeField(
		null = True,
	    blank = True,
	    default = None,
	    verbose_name = _('acceptance date')
	)
	confirm_date = models.DateTimeField(
		null = True,
	    blank = True,
	    default = None,
	    verbose_name = _('confirmation date')
	)
	active = models.BooleanField(
		default = True,
	    verbose_name = _('active')
	)

	objects = TransactionManager()

	@property
	def uid_str(self): return UUID(self.uid).hex
