# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from model_utils.managers import InheritanceManager
from django_extensions.db.fields import UUIDField
from django.db.models import Model
from django.conf import settings
from django.db import models
from uuid import UUID

__all__ = [ 'Operation', 'Giveaway', 'Trade', 'Purchase' ]

User = settings.AUTH_USER_MODEL
Book = 'bw_books.PhysicalBookCopy'
Currency = 'currencies.Currency'

class OperationManager(InheritanceManager):

	def active(self): return self.get_queryset().filter(active = True)
	def for_book(self, book): return self.get_queryset().filter(active = True, book = book)

class Operation(Model):

	class Meta(object):

		verbose_name = _('transaction')
		verbose_name_plural = _('transactions')
		app_label = 'bw_trans'
		db_table = 'transacts'

	uid = UUIDField(
		primary_key = True,
		db_index = True,
		auto = True,
		editable = False,
		verbose_name = _('unique transaction ID')
	)
	article = models.ForeignKey(
		Book,
		related_name = 'transacts',
		verbose_name = _('article')
	)
	create = models.DateTimeField(
		auto_now_add = True,
		auto_now = False,
		editable = False,
		verbose_name = _('creation date')
	)
	status = models.PositiveSmallIntegerField(
		default = 0,
		choices = ((_('Unset'), 0), (_('Accepted'), 1), (_('Confirmed'), 2))
	)
	active = models.BooleanField(
		default = True,
	    verbose_name = _('active')
	)

	objects = OperationManager()

	@property
	def uid_str(self): return UUID(self.uid).hex

class Giveaway(Operation):

	class Meta(object):

		verbose_name = _('giveaway transaction')
		verbose_name_plural = _('giveaway transactions')
		app_label = 'bw_trans'
		db_table = 'freebie_transacts'
class Trade(Operation):

	class Meta(object):

		verbose_name = _('trade transaction')
		verbose_name_plural = _('trade transactions')
		app_label = 'bw_trans'
		db_table = 'trade_transacts'

class Purchase(Operation):

	class Meta(object):

		verbose_name = _('purchase transaction')
		verbose_name_plural = _('purchase transactions')
		app_label = 'bw_trans'
		db_table = 'purchase_transacts'

	# TODO: When the linking between users and bank accounts is made, fill the appropiate fields
	# TODO: You must get an OpenExchangeRates API key to make the currencies work as intended (register in settings.py as OPENEXCHANGERATES_APP_ID)

	merchant_account = None
	currency = models.ForeignKey(
		Currency,
	    related_name = '+',
	    verbose_name = _('requested currency')
	)
	price = models.DecimalField(
		decimal_places = 2,
		max_digits = 8,
		verbose_name = _('price')
	)
