# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.crypto import constant_time_compare, salted_hmac
from django.utils.http import int_to_base36, base36_to_int
from datetime import date

class DeactivationTokenGenerator(object):

	def make_token(self, user): return self._make_token_with_timestamp(user, self._num_days(self._today()))
	def check_token(self, user, token):

		if not user.is_active: return False

		try: t36, hash = token.split('-')
		except ValueError: return False

		try: ts = base36_to_int(t36)
		except ValueError: return False

		if not constant_time_compare(self._make_token_with_timestamp(user, ts), token): return False
		return True
	def _make_token_with_timestamp(self, user, timestamp):

		t36 = int_to_base36(timestamp)
		key_salt = 'apps.bw_user.tokens.DeactivationTokenGenerator'

		login_timestamp = user.last_login.replace(microsecond = 0, tzinfo = None)
		value = '%s%s%s%s' % (user.id, user.uid, user.date_joined, login_timestamp)
		hash = salted_hmac(key_salt, value).hexdigest()[::2]

		return '%s-%s' % (t36, hash)
	def _num_days(self, dt): return (dt - date(2001, 1, 1)).days
	def _today(self): return date.today()
deactivation_token_generator = DeactivationTokenGenerator()
