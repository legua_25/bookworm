# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import ModelAdmin, register
from .models import User, Profile

@register(User)
class UserAdmin(ModelAdmin):

	date_hierarchy = 'date_joined'
	fieldsets = (
		(_('General'), { 'fields': [ 'uid', ('username', 'email'), 'password' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'is_active' ] })
	)
	readonly_fields = [ 'uid' ]
	search_fields = [ 'uid', 'email', 'username' ]

	def save_model(self, request, obj, form, change):

		if form.cleaned_data['password']: obj.set_password(form.cleaned_data['password'])
		obj.save()

@register(Profile)
class ProfileAdmin(ModelAdmin):

	fieldsets = (
		(_('General'), { 'fields': [ ('first_name', 'family_name') ] }),
		(_('Profile data'), { 'fields': [ 'gender', 'avatar' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'user' ] })
	)
