# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig

class UserConfig(AppConfig):

	verbose_name = _('User management')
	name = 'apps.bw_user'

	def ready(self):

		from djcelery.models import CrontabSchedule, WorkerState, PeriodicTask, TaskState, IntervalSchedule
		from django.contrib.auth.models import Group
		from django.contrib.admin import site

		site.unregister(CrontabSchedule)
		site.unregister(WorkerState)
		site.unregister(PeriodicTask)
		site.unregister(TaskState)
		site.unregister(IntervalSchedule)
		site.unregister(Group)

default_app_config = 'apps.bw_user.UserConfig'
