# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager as DjangoUserManager
from allauth.account.signals import user_logged_in
from django_extensions.db.fields import UUIDField
from imagekit.models import ProcessedImageField
from django.core.urlresolvers import reverse
from pilkit.processors import SmartResize
from django.dispatch import receiver
from django.db.models import Model
from django.conf import settings
from django.db import models
from uuid import UUID

class UserManager(DjangoUserManager):

	def active(self): return self.get_queryset().filter(is_active = True)
	def active_ratio(self):

		_all = self.all().count()
		_active = self.active().count()

		return (_all, _active, float(_all) / float(_active))
class User(AbstractBaseUser, PermissionsMixin):

	class Meta(object):

		unique_together = [ 'email', 'username' ]

		verbose_name = _('user')
		verbose_name_plural = _('users')
		app_label = 'bw_user'
		db_table = 'users'

	uid = UUIDField(
		auto = True,
		editable = False,
		verbose_name = _('user UUID identifier')
	)
	username = models.CharField(
		max_length = 64,
	    blank = False,
	    unique = True,
	    verbose_name = _('username')
	)
	email = models.EmailField(
		blank = False,
	    unique = True,
	    verbose_name = _('email address')
	)
	date_joined = models.DateTimeField(
		auto_now = False,
	    auto_now_add = True,
	    editable = False,
	    verbose_name = _('date joined')
	)
	is_active = models.BooleanField(
		default = True,
	    verbose_name = _('active')
	)

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = [ 'username' ]

	objects = UserManager()

	@property
	def is_staff(self): return self.is_superuser
	@property
	def uid_str(self): return UUID(self.uid).hex
	@property
	def unread_messages(self): return self.received_messages.filter(read_at__isnull = True, sender_deleted_at__isnull = True)

	def get_full_name(self): return self.profile.name if self.profile is not None else _('Unknown user')
	def get_short_name(self): return self.username or self.email or _('Unknown user')
	def get_absolute_url(self): return reverse('profile-view', kwargs = { 'uid': self.uid_str })
	def natural_key(self): return ( self.uid, self.username, self.email )

	def __str__(self): return '%s (%s)' % (self.username, self.email)

def _get_avatar(instance, filename): return './users/%s/avatar.jpg' % instance.user.uid_str
class Profile(Model):

	class Meta(object):

		verbose_name = _('user profile')
		verbose_name_plural = _('user profiles')
		app_label = 'bw_user'
		db_table = 'profiles'

	user = models.OneToOneField(
		User,
	    related_name = 'profile',
	    on_delete = models.CASCADE,
	    verbose_name = _('user')
	)
	first_name = models.CharField(
		max_length = 64,
	    default = '',
	    blank = True,
	    verbose_name = _('first name')
	)
	family_name = models.CharField(
		max_length = 64,
	    default = '',
	    blank = True,
	    verbose_name = _('family name')
	)
	gender = models.PositiveSmallIntegerField(
		default = 0,
	    choices = ((0, _('Unknown')), (1, _('Male')), (2, _('Female'))),
	    blank = True,
	    verbose_name = _('gender')
	)
	avatar = ProcessedImageField(
		processors = [ SmartResize(128, 128) ],
	    default = 'avatar.jpg',
	    blank = True,
		format = 'JPEG',
		upload_to = _get_avatar,
		verbose_name = _('avatar picture')
	)
	language = models.CharField(
		default = settings.LANGUAGE_CODE,
	    choices = settings.LANGUAGES,
	    max_length = 16,
	    verbose_name = _('language')
	)
	use_recommendations = models.BooleanField(
		default = True,
	    verbose_name = _('use recommendations')
	)

	@property
	def name(self):

		name = '%s %s' % (self.first_name, self.family_name)
		return name.rstrip()

	def get_absolute_url(self): return '%s/profile' % self.user.get_absolute_url()

	def __str__(self): return '%s\'s profile' % self.user.get_short_name()

@receiver(user_logged_in)
def add_user_profile(request, user, sociallogin = None, **kwargs):

	if not hasattr(user, 'profile') or user.profile is None:

		if sociallogin is not None:

			account = sociallogin.account
			if account.provider == 'facebook':

				first_name = account.extra_data['first_name']
				family_name = account.extra_data['last_name']
				avatar = "http://graph.facebook.com/%s/picture?width=128&height=128" % account.uid

				prof = Profile(user = user, first_name = first_name, family_name = family_name, avatar = avatar)

			elif account.provider == 'twitter':

				name = account.extra_data['name'].split()
				first_name = name[0]
				family_name = name[1]

				prof = Profile(user=user, first_name=first_name, family_name=family_name)

			else: prof = Profile(user = user)

		else: prof = Profile(user = user)

		prof.save()
		user.save()
