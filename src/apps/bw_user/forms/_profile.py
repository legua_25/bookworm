# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from allauth.account.models import EmailAddress
from allauth.account.adapter import get_adapter
from django.forms import Form, ValidationError
from django.contrib.messages import INFO
from django import forms

class AccountConfigForm(Form):

	username = forms.CharField(
		max_length = 64,
	    required = False,
	    widget = forms.TextInput(attrs = { 'placeholder': _('Username') })
	)
	password = forms.CharField(
		max_length = 128,
	    required = False,
	    widget = forms.PasswordInput(attrs = { 'placeholder': _('New password') })
	)
	confirm = forms.CharField(
		max_length = 128,
		required = False,
		widget = forms.PasswordInput(attrs = { 'placeholder': _('Confirm') })
	)
	gender = forms.TypedChoiceField(
		choices = ((1, _('Male')), (2, _('Female'))),
	    coerce = int,
	    empty_value = 0,
	    required = False
	)
	first_name = forms.CharField(
		max_length = 64,
	    required = False,
	    widget = forms.TextInput(attrs = { 'placeholder': _('First name') })
	)
	family_name = forms.CharField(
		max_length = 64,
		required = False,
		widget = forms.TextInput(attrs = { 'placeholder': _('Family name') })
	)

	def __init__(self, user, *args, **kwargs):

		super(AccountConfigForm, self).__init__(*args, **kwargs)
		self.user = user
		self._updates = {}

	def clean(self):

		user = self.user

		username = self.cleaned_data['username']
		if username is not None and username != user.username: self._updates['username'] = username

		password, confirm = (self.cleaned_data['password'], self.cleaned_data['confirm'])
		if password is not None and not user.check_password(password):
			if confirm != password: raise ValidationError('Provided passwords must match')

			self.cleaned_data['password'] = password

		gender = self.cleaned_data['gender']
		if gender is not 0 and gender != user.profile.gender:
			self._updates['gender'] = gender

		first_name, family_name = (self.cleaned_data['first_name'], self.cleaned_data['family_name'])
		if first_name is not None and first_name != user.profile.first_name:
			self._updates['first_name'] = first_name
		if family_name is not None and family_name != user.profile.family_name:
			self._updates['family_name'] = family_name

	def update(self, request):

		user = self.user

		if self._updates:

			out = {}

			if 'password' in self._updates:
				user.set_password(self._updates.pop('password'))
				out['password'] = True

			for field in self._updates:

				if field in ('first_name', 'family_name', 'gender'):
					setattr(user.profile, field, self._updates[field])
				else: setattr(user, field, self._updates[field])
				out[field] = self._updates[field]

			user.profile.save()
			user.save()

			return out

		return {}
