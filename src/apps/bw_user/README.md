# Template application
------------------------------

Use this template application to create other Django applications. Simply copy the entire structure and you'll be good to go.

This template includes all modules required for a complete application. The structure is as follows:

	/
	- admin:            Contains all ModelAdmins in order to manage models in Django's admin interface.
	- decorators:       Contains custom decorators which can simplify common tasks.
	- fields:           Contains custom model database fields.
	- fixtures:         Contains serialized data to prepopulate the database, preferably in JSON format.
	- forms:            Contains all forms and validators used throughout the application. Custom form fields are also included here.
	- management:       Contains all custom manage.py commands for common application actions.
		- commands:     Place all custom commands here, each in a separate file.
	- middleware:       Contains custom middleware for request/response preprocessing/postprocessing.
	- migrations:       Contains a history of database schema migrations for the application.
	- models:           Contains all ORM mappings (models) of database tables.
	- static:           Contains all static assets of the application, to be rendered by the views.
	- tasks:            Contains asynchronous tasks to be performed by this application using Celery.
	- templates:        Contains all webpage templates: internal organization is optional but recommended.
	- templatetags:     Contains custom template tags to add functionality to Django's template system.
	- test:             Contains all automated test modules.
	- views:            Contains all views, mey them be functions or class-based views.
	- __init__.py:      Marks this file as an application: do not modify.
	- signals.py:       Contains custom signals for the Django signal dispatcher, in order to process specific events.

Feel free to edit this README to match the new application's needs.