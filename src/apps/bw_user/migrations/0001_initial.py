# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.bw_user.models
from django.conf import settings
import django.utils.timezone
import imagekit.models.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('uid', django_extensions.db.fields.UUIDField(verbose_name='user UUID identifier', editable=False, name=b'uid', blank=True)),
                ('username', models.CharField(unique=True, max_length=64, verbose_name='username')),
                ('email', models.EmailField(unique=True, max_length=75, verbose_name='email address')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='date joined')),
                ('is_active', models.BooleanField(default=True, verbose_name='active')),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'db_table': 'users',
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(default='', max_length=64, verbose_name='first name', blank=True)),
                ('family_name', models.CharField(default='', max_length=64, verbose_name='family name', blank=True)),
                ('gender', models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='gender', choices=[(0, 'Unknown'), (1, 'Male'), (2, 'Female')])),
                ('avatar', imagekit.models.fields.ProcessedImageField(default='avatar.jpg', upload_to=apps.bw_user.models._get_avatar, verbose_name='avatar picture', blank=True)),
                ('language', models.CharField(default='en-us', max_length=16, verbose_name='language', choices=[('en', 'English'), ('es', 'Spanish')])),
                ('use_recommendations', models.BooleanField(default=True, verbose_name='use recommendations')),
                ('user', models.OneToOneField(related_name='profile', verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'profiles',
                'verbose_name': 'user profile',
                'verbose_name_plural': 'user profiles',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='user',
            unique_together=set([('email', 'username')]),
        ),
    ]
