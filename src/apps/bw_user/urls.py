# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import include, patterns, url

urlpatterns = patterns('',

	url(r'(?P<uid>[0-9a-f]{32})/', include(patterns('apps.bw_user.views',

		url(r'^$', 'profile', name = 'profile-view'),
		url(r'^(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/deactivate/$', 'account_deactivate', name = 'user-deactivate'),
	    url(r'^permissions/', 'perms',  name = 'user-permissions'),

	))),
    url(r'^config/$', 'apps.bw_user.views.account_config', name = 'user-config'),
    url(r'^report/$', 'apps.bw_user.views.user_report', name = 'user-report'),
    url(r'^', include('allauth.urls')),
    url(r'^$', 'apps.bw_user.views.account_redirect', name = 'account-redirect')

)
