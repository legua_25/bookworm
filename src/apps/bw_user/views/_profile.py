# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render_to_response, RequestContext
from ..tokens import deactivation_token_generator as tokens
from json import dumps as to_json, loads as from_json
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.models import Permission
from apps.decorators import ajax_required
from apps.bw_books.models import BookCopy
from django.contrib.auth import logout
from django.views.generic import View
from ..forms import AccountConfigForm
from ..models import User
from uuid import UUID

class ProfileView(View):

	def get(self, request, uid = None, *args, **kwargs):

		uid = str(UUID(uid))

		try: u = User.objects.get(uid__iexact = uid, is_active = True)
		except User.DoesNotExist: u = None
		else:

			books = BookCopy.objects.filter(active = True, owner = u).select_subclasses()

		return render_to_response('users/profile.html', context_instance = RequestContext(request, locals()))
profile = ProfileView.as_view()

class AccountDeactivationView(View):

	@method_decorator(login_required)
	def post(self, request, uid = None, token = None, *args, **kwargs):

		try:

			uid = str(UUID(uid))
			if uid is not request.user.uid: raise ValueError('Attempting to deactivate user different to oneself')

			user = request.user
		except ValueError: pass
		else:

			if tokens.check_token(user, token):

				logout(request)

				user.is_active = False
				user.save()

				return HttpResponseRedirect(reverse_lazy('index'))

			else:

				# The user was already deactivated or the session was tampered with. Nullify request
				pass
account_deactivate = AccountDeactivationView.as_view()

class AccountConfigurationView(View):

	@method_decorator(login_required)
	def get(self, request, *args, **kwargs):

		user = request.user
		form = AccountConfigForm(user, initial = {
			'username': user.username,
			'first_name': user.profile.first_name,
			'family_name': user.profile.family_name,
			'gender': user.profile.gender
		})
		prof = request.user.profile

		return render_to_response('users/config.html', context_instance = RequestContext(request, locals()))
	@method_decorator(ajax_required)
	@method_decorator(login_required)
	def post(self, request, *args, **kwargs):

		user = request.user
		form = AccountConfigForm(user, request.POST, initial = {
			'username': user.username,
		    'first_name': user.profile.first_name,
		    'family_name': user.profile.family_name,
		    'gender': user.profile.gender
		})

		if form.is_valid():
			return HttpResponse(to_json(form.update(request)), content_type = 'application/json')

		return HttpResponse(to_json({}), content_type = 'application/json')
account_config = AccountConfigurationView.as_view()

class AccountPermissionView(View):

	@method_decorator(ajax_required)
	@method_decorator(login_required)
	@method_decorator(user_passes_test(lambda u: u.is_staff))
	def post(self, request, uid = None, *args, **kwargs):

		# "perms" is a dict with the form { 'attribute': True|False [, ...] }
		perms = from_json(request.POST.get('perm_changes', {}))
		if bool(perms) is True:

			try:

				user = User.objects.get(uid = str(UUID(uid)))
				for key in ( 'bw_books.add_book_copy', 'bw_books.add_author', 'postman.add_message' ):

					if key in perms:

						perm = Permission.objects.get(codename = key)

						if perms[key] is True and not user.has_perm(key): user.user_permissions.add(perm)
						else: user.user_permissions.remove(perm)

				user.save()

				return HttpResponse(to_json({
					'uid': uid,
					'status': True
				}), content_type = 'application/json')

			except User.DoesNotExist: pass

		return HttpResponseBadRequest
perms = AccountPermissionView.as_view()

