# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views.generic import View

class AccountRedirectView(View):

	def get(self, request, *args, **kwargs):
		return HttpResponseRedirect(reverse('profile-view', kwargs = { 'uid': request.user.uid_str }))
account_redirect = login_required(AccountRedirectView.as_view())
