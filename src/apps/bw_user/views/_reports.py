# -*- coding: utf-8 -*-
from __future__ import unicode_literals, with_statement
from django.contrib.auth.decorators import login_required, user_passes_test
from apps.bw_books.models import Comment, Recommendation, Review
from django.shortcuts import render_to_response, RequestContext
from django.utils.decorators import method_decorator
from django.contrib.auth import get_user_model
from django.db.transaction import atomic
from django.views.generic import View
from django.db.models import *

User = get_user_model()

class UserReportView(View):

	@method_decorator(login_required)
	@method_decorator(user_passes_test(lambda u: u.is_staff))
	def get(self, request, *args, **kwargs):

		# We must lock down the DB to prevent changes and provide an exact report method.
		# This will also speed up the queries dramatically.
		with atomic():

			active_ratio = User.objects.active_ratio()

			if Comment.objects.exists():

				# Filter all users by comment count, selecting top 10 (use annotations to add comment count).
				top_commenters = [ {
					'user': u,
				    'comment_count': u.comment_count
				} for u in User.objects.active().annotate(comment_count = Count('comments')).order_by('-comment_count')[:10] ]
			else: top_commenters = []

			if Review.objects.exists():

				# Filter all users by review count, selecting top 10 (use annotations to add review count).
				top_reviewers = [{
					'user': u,
					'review_count': u.review_count
				} for u in User.objects.active().annotate(review_count = Count('reviews')).order_by('-review_count')[:10] ]
			else: top_reviewers = []

			if Recommendation.objects.exists():

				# Filter all users by recommendation count, selecting top 10 (use annotations to add recommendation count).
				top_recommenders = [{
					'user': u,
					'recommends': u.recommendations
				} for u in User.objects.active().annotate(recommends = Count('recommendations')).order_by('-recommends')[:10] ]
			else: top_recommenders = []

		return render_to_response('users/user_report.html', context_instance = RequestContext(request, locals()))
user_report = UserReportView.as_view()
