# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseBadRequest

def ajax_required(view):
	""" From <"https://djangosnippets.org/snippets/771/"> """

	def wrap(request, *args, **kwargs):

		if not request.is_ajax(): return HttpResponseBadRequest()

		return view(request, *args, **kwargs)

	wrap.__doc__ = view.__doc__
	wrap.__name__ = view.__name__

	return wrap
