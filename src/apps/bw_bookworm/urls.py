from django.conf.urls import patterns, url, include

urlpatterns = patterns('apps.bw_bookworm.views',

    url(r'^', include(patterns('apps.bw_bookworm.views',

        url(r'^books/$', 'report_books_view.report_books', name = 'report_books')

    ))),

)