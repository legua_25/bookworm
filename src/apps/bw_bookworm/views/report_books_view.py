# -*- coding: utf-8 -*-
from __future__ import unicode_literals, with_statement
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from apps.bw_books.models import Book, Author, PhysicalBookCopy, DigitalBookCopy, BookCopy, Genre, GenreTag
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render_to_response, RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from django.db.models import Q
from django.db.transaction import atomic
from re import match
import operator
import datetime

class BookReportView(generic.View):

    def get(self, request, *args, **kwargs):

        if not request.user.is_staff:
            return HttpResponseForbidden()

        with atomic():

            flagged_books = Book.objects.filter(Q(flags__gt = 0)).order_by('flags')[::-1]
            flagged_copies = BookCopy.objects.filter(flags__gt = 0).order_by('flags')[::-1]
            n_fl_b = len(flagged_books)
            n_fl_c = len(flagged_copies)

            book_list = Book.objects.all()
            copy_list = BookCopy.objects.all()
            physical_copies = PhysicalBookCopy.objects.all()
            digital_copies = DigitalBookCopy.objects.all()

            n_active = len(copy_list.filter(active = 1))
            n_inactive = len(copy_list.filter(active = 0))

            n_di = len(digital_copies)
            n_ph = len(physical_copies)

            n_books = len(Book.objects.all())
            copy_type_perc = []
            i = 0
            for b in book_list:
                if len(b.book_copies.all()) > 0:
                    copy_type_perc.append([i, b.title, len(physical_copies.filter(book = b)), len(digital_copies.filter(book = b)), len(b.book_copies.all())])
                    i+=1

            book_additions = [] #Fuck this
            for i in range(0, 12):
                book_additions.append(len(BookCopy.objects.filter(addition_date__year = datetime.datetime.now().year).filter(addition_date__month = datetime.datetime.now().month)))

            #n_trade = len(PhysicalBookCopy.objects.filter(for_trade = 1))
            #n_sale = len(PhysicalBookCopy.objects.filter(for_sale = 1))

            n_genres = len(Genre.objects.all())
            genres = Genre.objects.all()
            tag_list = []
            i = 0
            for genre in genres:
                num_tags = len(GenreTag.objects.filter(genre = genre))
                num_tag_books = len(GenreTag.objects.filter(genre = genre).values('book').distinct())
                tag_list.append({'i': i, 'genre': genre, 'num_tag_books': num_tag_books, 'num': num_tags})
                i+=1
            tag_list.sort(key=operator.itemgetter('num'), reverse=True)

            return render_to_response('reports_templates/book_report.html', context_instance = RequestContext(request, locals()))

report_books = login_required(BookReportView.as_view())