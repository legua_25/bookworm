# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render_to_response, RequestContext
from apps.bw_books.models import Recommendation, BookCopy
from django.views.generic import View

class IndexView(View):

	def get(self, request, *args, **kwargs):

		has_books = BookCopy.objects.exists()

		if has_books:
			_user = request.user

			if request.user is not None and request.user.is_authenticated():

				if request.user.profile.use_recommendations: _titles = BookCopy.objects.get_recommended_for(_user)
				else: _titles = [ book for book in BookCopy.objects.filter(active = True).exclude(owner = _user).order_by('-addition_date')[:16] ]
			else: _titles = [ book for book in BookCopy.objects.filter(active = True).order_by('-addition_date')[:16] ]

			has_books = bool(_titles)

			titles = []
			for i in range(0, len(_titles), 4):
				titles.append({ 'values': _titles[i:i + 4], 'active': i == 0 })

		return render_to_response('bookworm/index.html', context_instance = RequestContext(request, locals()))
index = IndexView.as_view()
