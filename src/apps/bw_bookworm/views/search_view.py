# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from apps.bw_books.models import Book, Author, PhysicalBookCopy, DigitalBookCopy, BookCopy, Genre, GenreTag
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, RequestContext
from django.db.models import Q
from re import match

class SearchView(generic.View):

	def post(self, request, *args, **kwargs):

		search_query = request.POST['query']
		search_option = request.POST.get('search_option', 'all')

		if match(r'((978[\--– ])?[0-9][0-9\--– ]{10}[\--– ][0-9xX])|((978)?[0-9]{9}[0-9Xx])', search_query):
			search_option = 'isbn'

		if search_option == 'title':

			books = [{
		        'book': b,
		        'physical': PhysicalBookCopy.objects.filter(active = True, book = b).count(),
		        'digital': DigitalBookCopy.objects.filter(active = True, book = b).count()
	        } for b in Book.objects.active().filter(title__icontains = search_query)]

			has_results = bool(books)

		elif search_option == 'authors':

			authors = Author.objects.active()
			for word in search_query.split():
				authors = authors.filter(Q(first_name__icontains = word) | Q(family_name__icontains = word))

			authors = [{
			   'author': a,
			   'count': a.titles.all().count()
			} for a in authors]

			has_results = bool(authors)

		elif search_option == 'genres':

			genres = [ {
				'genre': g,
				'count': GenreTag.objects.active().filter(genre = g).count()
			} for g in Genre.objects.filter(name__icontains = search_query, active = True) ]

			has_results = bool(genres)

		elif search_option == 'isbn':

			copies = BookCopy.objects.active().filter(isbn = search_query).select_subclasses()
			has_results = bool(copies)

		else: # search all

			authors = Author.objects.active()
			for word in search_query.split():
				authors = authors.filter(Q(first_name__icontains = word) | Q(family_name__icontains = word))

			authors = [ {
				'author': a,
			    'count': a.titles.all().count()
			} for a in authors ]

			books = [ {
				'book': b,
				'physical': PhysicalBookCopy.objects.filter(active = True, book = b).count(),
				'digital': DigitalBookCopy.objects.filter(active = True, book = b).count()
			} for b in Book.objects.active().filter(title__icontains = search_query) ]

			genres = [{
			  'genre': g,
			  'count': GenreTag.objects.active().filter(genre = g).count()
			} for g in Genre.objects.filter(name__icontains = search_query, active = True)]

			has_results = bool(genres) or bool(books) or bool(authors)

		return render_to_response('search_templates/search_results.html', context_instance = RequestContext(request, locals()))

search = SearchView.as_view()
