var search_option = 'all';

document.getElementById("search_all").onclick = function() { search_option = 'all'; set_op(); };
document.getElementById("search_title").onclick = function() { search_option = 'title'; set_op(); };
document.getElementById("search_authors").onclick = function() { search_option = 'authors'; set_op(); };
document.getElementById("search_genres").onclick = function() { search_option = 'genres'; set_op(); };
document.getElementById("search_isbn").onclick = function() { search_option = 'isbn'; set_op(); };

function set_op() {
    document.getElementById("search_option").setAttribute("value", search_option);
    document.getElementById("all-search").click();
}