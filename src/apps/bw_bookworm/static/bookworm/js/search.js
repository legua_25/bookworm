
var search_option = 'all';

document.getElementById("all-search").onclick = search;
document.getElementById("id_search").onkeydown = checkKey;
document.getElementById("search_all").onclick = function() { search_option = 'all'; search(); };
document.getElementById("search_title").onclick = function() { search_option = 'title'; search(); };
document.getElementById("search_authors").onclick = function() { search_option = 'authors'; search(); };
document.getElementById("search_genres").onclick = function() { search_option = 'genres'; search(); };
document.getElementById("search_isbn").onclick = function() { search_option = 'isbn'; search(); };

function checkKey(e) {
    if(e.which == 13) {
        search();
    }
}

function search() {
    $.ajax({
        type: 'POST',
        url: "/search/", //- action form
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            query: $('#id_search').val(),
            search_option: search_option
        },
        success: function(data){
            document.getElementById('search_results').innerHTML = data;
        }
    });
}