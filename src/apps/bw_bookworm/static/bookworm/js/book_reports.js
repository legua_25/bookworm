window.onload = function() {

    n_act = document.getElementById("n_act").innerHTML;
    n_inact = document.getElementById("n_inact").innerHTML;
    new Chartkick.PieChart("act_inact_chart", [["Active copies", n_act],["Inactive copies", n_inact]]);

    n_ph = document.getElementById("n_ph").innerHTML;
    n_di = document.getElementById("n_di").innerHTML;
    new Chartkick.PieChart("ph_di_chart", [["Physical copies", n_ph],["Digital copies", n_di]]);

    n_books = document.getElementById("n_books").innerHTML;
    book_cpy = []
    for (var i = 0; i < n_books; i++) {
        book_cpy.push([document.getElementById("book_"+i).innerHTML, document.getElementById("ph_cpy_"+i).innerHTML / document.getElementById("tt_cpy_"+i).innerHTML])
    }
    new Chartkick.BarChart("book_cpy_chart", book_cpy);

    n_tr = document.getElementById("n_tr").innerHTML;
    n_sa = document.getElementById("n_sa").innerHTML;
    new Chartkick.BarChart("tr_sa_chart", [["For trade", n_tr],["For sale", n_sa],["Total", n_ph]]);

    n_genres = document.getElementById("n_genres").innerHTML;
    genres_books = []
    genres_tags = []
    for (var i = 0; i < n_genres; i++) {
        genres_books.push([document.getElementById("genre_"+i).innerHTML, document.getElementById("genre_books_"+i).innerHTML])
        genres_tags.push([document.getElementById("genre_"+i).innerHTML, document.getElementById("genre_tags_"+i).innerHTML])
    }
    new Chartkick.BarChart("genres_chart", genres_books);
    new Chartkick.BarChart("tags_chart", genres_tags);
}