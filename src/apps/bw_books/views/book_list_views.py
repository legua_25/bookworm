from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from apps.bw_books.models import Book
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, RequestContext
from re import match

class BookListView(generic.View):

    def get(self, request, *args, **kwargs):

        page = int(request.GET.get('page', 1)) - 1
        start, end = ((page * 50), (page * 50) + 50)

        book_list = Book.objects.active()[start:end]

        return render_to_response('books_templates/book_list.html', context_instance = RequestContext(request, locals()))

    def post(self, request, *args, **kwargs):

        search = request.POST['search']
        search_option = request.POST['search_option']

        return render_to_response('books_templates/book_list.html', context_instance = RequestContext(request, locals()))

list_books = BookListView.as_view()
