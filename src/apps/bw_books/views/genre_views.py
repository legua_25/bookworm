# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from apps.bw_books.models import Book, Genre, GenreTag
from django.contrib.auth import get_user
import operator
from django.views import generic
from uuid import UUID
from django.http import HttpResponseBadRequest
from django.shortcuts import render_to_response, RequestContext
from django.contrib.auth.decorators import login_required

class GetGenreView(generic.View):

    def get(self, request, *args, **kwargs):

        book = Book.objects.get(uid = request.GET['book_id'])
        tags = GenreTag.objects.filter(book = book).order_by('genre_id')

        tag_list = []
        for tag in tags:
            elem = {'genre': Genre.objects.get(uid = tag.genre_id), 'num': len(GenreTag.objects.filter(genre_id = tag.genre_id, book = book))}
            if elem not in tag_list:
                tag_list.append(elem)
        tag_list.sort(key=operator.itemgetter('num'), reverse=True)
        tag_list = tag_list[:5]

        return render_to_response('books_templates/book_genres.html', context_instance = RequestContext(request, locals()))

get_genres = GetGenreView.as_view()

class AddGenreView(generic.View):

    def post(self, request, *args, **kwargs):
        genre_name = request.POST.get('genre', '').strip()
        if bool(genre_name) is False or 'book_id' not in request.POST:
            return HttpResponseBadRequest()

        book = Book.objects.get(uid = request.POST['book_id'])
        genre, create = Genre.objects.get_or_create(name = genre_name)
        user = get_user(request)
        genre_tag = GenreTag.objects.get_or_create(book = book, genre = genre, user = user)

        tags = GenreTag.objects.filter(book = book).order_by('genre_id')

        tag_list = []
        for tag in tags:
            elem = {'genre': Genre.objects.get(uid = tag.genre_id), 'num': len(GenreTag.objects.filter(genre = tag.genre, book = book))}
            if elem not in tag_list:
                tag_list.append(elem)
        tag_list.sort(key=operator.itemgetter('num'), reverse=True)
        tag_list = tag_list[:5]

        return render_to_response('books_templates/book_genres.html', context_instance = RequestContext(request, locals()))

add_genre = login_required(AddGenreView.as_view())

class GenreView(generic.View):

    def get(self, request, genre_id = None, *args, **kwargs):

        genre = Genre.objects.get(uid = str(UUID(genre_id)))

        book_list = []
        tag_list = GenreTag.objects.filter(genre = genre)
        for tag in tag_list:
            book = Book.objects.get(uid = tag.book.uid)
            elem = {'book': book, 'num': len(GenreTag.objects.filter(genre = tag.genre, book = book))}
            if elem not in book_list: #and elem['num'] > 1:
                book_list.append(elem)
        book_list.sort(key=operator.itemgetter('num'), reverse=True)
        book_list = book_list[:50]

        return render_to_response('books_templates/genre.html', context_instance = RequestContext(request, locals()))

genre_info = GenreView.as_view()