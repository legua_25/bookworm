# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from apps.bw_books.models import Book, BookCopy, PhysicalBookCopy, Review, Rating, Comment, Recommendation
from ..forms.book_forms import AddPhysicalBookForm, AddBookCopyForm
from ..forms import CopyEditForm
from django.contrib.auth import get_user
from django.views import generic
from uuid import UUID
from django.http import HttpResponseBadRequest, HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render_to_response, RequestContext
from django.contrib.auth.decorators import login_required

class GetReviewsView(generic.View):

	def get(self, request, *args, **kwargs):

		book = Book.objects.get(uid = request.GET['book_id'])
		reviews = Review.objects.active().filter(book = book, approved = True)

		return render_to_response('books_templates/book_reviews.html', context_instance = RequestContext(request, locals()))
get_reviews = GetReviewsView.as_view()

class AddReviewView(generic.View):

	def post(self, request, *args, **kwargs):
		msg = request.POST.get('review_content', '').strip()

		if bool(msg) is True:
			book = Book.objects.get(uid = request.POST.get('book_id'))
			user = get_user(request)
			review, create = Review.objects.get_or_create(book = book, reviewer = user, content = msg)

			reviews = Review.objects.active().filter(book = book, approved = True)

			response_msg = _('Thank you for adding your review. Your review will be visible as soon as an administrator approves it.')

			return render_to_response('books_templates/book_reviews.html', context_instance = RequestContext(request, locals()))

		return HttpResponseBadRequest()
add_review = login_required(AddReviewView.as_view())

class RateBook(generic.View):

	def get(selfs, request, *args, **kwargs):

		if 'book_id' in request.GET:
			book = Book.objects.get(uid = request.GET.get('book_id'))
			user = get_user(request)
			rating = Rating.objects.filter(book = book, rater = user)
			if len(rating) > 0:
				number_recommendations = rating[0].rating
			else:
				number_recommendations = -1

			return render_to_response('books_templates/number_recommendations.html', context_instance = RequestContext(request, locals()))

		return HttpResponseBadRequest()

	def post(self, request, *args, **kwargs):

		if 'book_id' in request.POST and 'rating' in request.POST and 0 <= int(request.POST.get('rating')) <= 5:
			book = Book.objects.get(uid = request.POST.get('book_id'))
			user = get_user(request)
			rating = Rating.objects.filter(book = book, rater = user)
			if len(rating) is 0: rating = Rating(book = book, rater = user)
			else: rating = rating[0]
			rating.rating = request.POST.get('rating')
			rating.save()

			number_recommendations = Rating.objects.average_for(book)
			return render_to_response('books_templates/number_recommendations.html', context_instance = RequestContext(request, locals()))

		return HttpResponseBadRequest()
rate_book = login_required(RateBook.as_view())

class RecommendBook(generic.View):

	def post(self, request, *args, **kwargs):

		if 'book_id' in request.POST:
			book = Book.objects.get(uid = request.POST.get('book_id'))
			user = get_user(request)
			recommendation, create = Recommendation.objects.get_or_create(book = book, recommender = user)
			number_recommendations = book.number_recommendations
			return render_to_response('books_templates/number_recommendations.html', context_instance = RequestContext(request, locals()))

		return HttpResponseBadRequest
recommend = login_required(RecommendBook.as_view())

class GetCommentsView(generic.View):

	def get(self, request, *args, **kwargs):

		copy = BookCopy.objects.get(uid = request.GET['copy_id'])
		comments = Comment.objects.active().filter(book = copy)

		return render_to_response('books_templates/book_copy_comments.html', context_instance = RequestContext(request, locals()))
get_comments = GetCommentsView.as_view()

class AddCommentView(generic.View):

	def post(self, request, *args, **kwargs):

		msg = request.POST.get('comment_content', '').strip()
		if bool(msg) is True:
			copy = BookCopy.objects.get(uid = request.POST['copy_id'])
			user = get_user(request)
			comment, create = Comment.objects.get_or_create(book = copy, commenter = user, content = msg)

			comments = Comment.objects.active().filter(book = copy)
			return render_to_response('books_templates/book_copy_comments.html', context_instance = RequestContext(request, locals()))

		return HttpResponseBadRequest()
add_comment = login_required(AddCommentView.as_view())

class EditBookView(generic.View):

	def get(self, request, book_id = None, book_copy_id = None, *args, **kwargs):

		copy = BookCopy.objects.get_subclass(uid = str(UUID(book_copy_id)))
		form = CopyEditForm(copy)

		if (not request.user == copy.owner and not request.user.is_staff) or book_id != copy.book.uid_str:
			book = copy.book
			return render_to_response('books_templates/book_copy_details.html', context_instance = RequestContext(request, locals()))

		return render_to_response('books_templates/book_copy_edit.html', context_instance = RequestContext(request, locals()))

	def post(self, request, book_id, book_copy_id = None, *args, **kwargs):

		copy = BookCopy.objects.get_subclass(uid = str(UUID(book_copy_id)))

		if (not request.user == copy.owner and not request.user.is_staff) or book_id != copy.book.uid_str:
			return HttpResponseBadRequest()

		form = CopyEditForm(copy, data = request.POST, files = request.FILES)
		if form.is_valid():
			
			form.save()
			return HttpResponseRedirect(reverse_lazy('details', args = [ copy.book.uid_str, copy.uid_str ]))

		return render_to_response('books_templates/book_copy_edit.html', context_instance = RequestContext(request, locals()))

edit_book = login_required(EditBookView.as_view())

class RemoveBookView(generic.View):

	def get(self, request, *args, **kwargs):

		user = get_user(request)
		book_copy = BookCopy.objects.get(uid = request.GET['copy_id'])
		if book_copy.owner == user or user.is_staff:
			book_copy.active = False
			book_copy.save()
			return HttpResponseRedirect(reverse_lazy('profile-view', args=[get_user(request).uid_str]))

		return HttpResponseBadRequest
remove_book = login_required(RemoveBookView.as_view())

class FlagBookView(generic.View):

	def post(self, request, *args, **kwargs):

		if 'book_id' in request.POST:
			book = Book.objects.get(uid = request.POST['book_id'])
			book.flags += 1
			book.save()
		elif 'copy_id' in request.POST:
			copy = BookCopy.objects.get(uid = request.POST['copy_id'])
			copy.flags += 1
			copy.save()
		else:
			return HttpResponseBadRequest

		return render_to_response('books_templates/flag_response.html', context_instance = RequestContext(request, locals()))

flag = login_required(FlagBookView.as_view())
