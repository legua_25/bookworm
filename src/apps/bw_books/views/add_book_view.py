from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.shortcuts import render
from apps.bw_books.models import Book, Author, DigitalBookCopy, PhysicalBookCopy, BookCopy
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from ..forms.book_forms import AddBookForm, AddAuthorForm, AddDigitalBookForm, AddPhysicalBookForm, AddBookCopyForm
from django.shortcuts import render_to_response, RequestContext
from django.contrib.auth.decorators import login_required
import os
from uuid import UUID
from apps.bw_trans.forms.trans_forms import PurchaseBookForm
from apps.bw_trans.models import Trade, Purchase, Giveaway, Transaction

class AddBookView(generic.View):

	def get(self, request, *args, **kwargs):

		book_form = AddBookForm()
		author_form = AddAuthorForm()
		copy_form = AddBookCopyForm(initial = { 'pages': 0 })
		physical_form = AddPhysicalBookForm()
		digital_form = AddDigitalBookForm()
		purchase = PurchaseBookForm(instance = Purchase())

		return render_to_response('books_templates/book_add.html', context_instance = RequestContext(request, locals()))
	def post(self, request, *args, **kwargs):

		# Gets or creates a new book
		book = self._get_or_create_book(request.POST)
		if book is not None:

			_type = 'physical' if 'physical' in request.POST else 'digital'
			copy = self._create_book_copy(book, request.user, _type, request.POST, request.FILES)

			if copy is not None:
				return HttpResponseRedirect(reverse_lazy('details', args = [ book.uid_str, copy.uid_str ]))

		book_form = AddBookForm(request.POST)
		author_form = AddAuthorForm(request.POST)
		copy_form = AddBookCopyForm(request.POST, request.FILES)
		digital_form = AddDigitalBookForm(request.POST, request.FILES)
		physical_form = AddPhysicalBookForm(request.POST)
		purchase = PurchaseBookForm(request.POST)

		return render_to_response('books_templates/book_add.html', context_instance = RequestContext(request, locals()))

	def _create_transactions_for(self, copy, data):

		if 'trade' in data:

			t = Trade(article = copy, status = 0)
			t.save()
			copy.transacts.add(t)
		if 'giveaway' in data:

			t = Giveaway(article = copy, status = 0)
			t.save()
			copy.transacts.add(t)
		if 'purchase' in data:

			form = PurchaseBookForm(data, instance = Purchase(article = copy, status = 0))
			if form.is_valid():

				t = form.instance
				t.save()
				copy.transacts.add(t)
			else: return None

		copy.save()
		return copy
	def _create_book_copy(self, book, owner, copy_type, data, files):

		if copy_type == 'digital':

			copy = DigitalBookCopy(book = book, owner = owner)
			form = AddDigitalBookForm(data, files, instance = copy)

			if form.is_valid():

				form.save()
				return form.instance
		else:

			copy = PhysicalBookCopy(book = book, owner = owner)
			form = AddPhysicalBookForm(data, files, instance = copy)

			if form.is_valid():

				form.save()
				return self._create_transactions_for(form.instance, data)
	def _get_or_create_book(self, data):

		form = AddBookForm(data)
		if form.is_valid():

			# Obtain author names
			first_names, family_names = dict(data).get('first_name', []), dict(data).get('family_name', [])
			if bool(first_names) is True:

				temp = [(first_names[i], family_names[i]) for i in range(len(first_names))]
				temp = list(filter(lambda a: bool(a[0]), temp))
				first_names, family_names = [a[0] for a in temp], [a[1] for a in temp]

				# Add anonymous author if no author(s) is(are) present
				if bool(first_names) is False:
					first_names.append('Anonymous')
					family_names.append('')

			# Create author-fetching query
			query = None
			for i in range(len(first_names)):

				subquery = { }
				if bool(first_names[i]) is True: subquery['first_name__iexact'] = first_names[i]
				if bool(family_names[i]) is True: subquery['family_name__iexact'] = family_names[i]

				if query is None:
					query = Q(**subquery)
				else:
					query = query & Q(**subquery)

			# Get all books in database with the provided title (check for matches)
			books = Book.objects.filter(title__iexact = data['title'])

			if bool(first_names) is True:

				# If there is a book with all the provided authors, we have a match
				for b in books:

					author_count = b.authors.filter(query).count()
					if author_count > 0:
						return b

			# No book was found: create a new one
			book = form.instance
			book.update_description()
			book.save()

			for i in range(len(first_names)):

				subquery = {}
				if bool(first_names[i]) is True: subquery['first_name'] = first_names[i]
				if bool(family_names[i]) is True: subquery['family_name'] = family_names[i]

				author, __ = Author.objects.get_or_create(**subquery)
				author.update_biography()
				author.save()
				book.authors.add(author)

			book.save()
			return book

		return None
add_book = login_required(AddBookView.as_view())
