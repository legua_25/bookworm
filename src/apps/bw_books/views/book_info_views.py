# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.views import generic
from django.http import HttpResponse
from django.shortcuts import render_to_response, RequestContext
from apps.bw_books.models import Book, Author, BookCopy, DigitalBookCopy, PhysicalBookCopy, Review, Rating, Recommendation
from django.conf import settings
from uuid import UUID
import os
from django.contrib.auth import get_user

class ReadDigitalBookView(generic.DetailView):
	model = DigitalBookCopy

	def get(self, request, book_copy_id = None, *args, **kwargs):

		self.model = DigitalBookCopy.objects.get(uid = str(UUID(book_copy_id)))

		with open(os.path.join(settings.MEDIA_ROOT, self.model.pdf.name), 'r') as pdf:
			response = HttpResponse(pdf.read(), content_type='application/pdf')
			return response
read = ReadDigitalBookView.as_view()

class BookInfoView(generic.View):

	def get(self, request, book_id = None, *args, **kwargs):

		book = Book.objects.get(uid = str(UUID(book_id)))

		number_recommendations = book.number_recommendations
		book_copies = BookCopy.objects.filter(book = book, active = True).select_subclasses()
		rating = Rating.objects.average_for(book)

		user = get_user(request)
		if(user.is_authenticated()):
			rev = Review.objects.filter(reviewer = user, book = book)
			rec = Recommendation.objects.filter(recommender = user, book = book)

		return render_to_response('books_templates/book_info.html', context_instance = RequestContext(request, locals()))
book_info = BookInfoView.as_view()

class AuthorView(generic.View):

	def get(self, request, author_id = None, *args, **kwargs):

		author = Author.objects.get(uid = str(UUID(author_id)))
		book_list = Book.objects.filter(authors__in = [ author ])
		return render_to_response('books_templates/author.html', context_instance = RequestContext(request, locals()))
author_info = AuthorView.as_view()

class BookCopyDetailsView(generic.View):

	def get(self, request, book_id = None, book_copy_id = None, *args, **kwargs):

		user = get_user(request)
		book = Book.objects.get(uid = str(UUID(book_id)))
		try: copy = PhysicalBookCopy.objects.get(uid = str(UUID(book_copy_id)))
		except PhysicalBookCopy.DoesNotExist: copy = DigitalBookCopy.objects.get(uid = str(UUID(book_copy_id)))

		if book.uid_str != copy.book.uid_str:
			book = copy.book

		return render_to_response('books_templates/book_copy_details.html', context_instance = RequestContext(request, locals()))
details = BookCopyDetailsView.as_view()
