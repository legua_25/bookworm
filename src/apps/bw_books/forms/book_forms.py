# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from time import strftime
from django.conf import settings
from apps.bw_books.models import Book, Author, DigitalBookCopy, PhysicalBookCopy, BookCopy
from django import forms
from django.core.validators import RegexValidator
from imagekit.forms import ProcessedImageField
from pilkit.processors import SmartResize

class AddBookForm(forms.ModelForm):

	class Meta:
		model = Book
		fields = ('title',)

	title = forms.CharField(
		max_length = 64,
		required = True,
		widget = forms.TextInput(attrs = { 'placeholder': _('Title') })
	)

class AddAuthorForm(forms.ModelForm):

	class Meta:
		model = Author
		fields = ('first_name','family_name',)

	first_name = forms.CharField(
		max_length = 64,
		required = False,
		widget = forms.TextInput(attrs = { 'placeholder': _('First name') })
	)
	family_name = forms.CharField(
		max_length = 64,
		required = False,
		widget = forms.TextInput(attrs = { 'placeholder': _('Family name') })
	)

class AddBookCopyForm(forms.ModelForm):

	class Meta:
		model = BookCopy
		fields = ('isbn', 'language', 'description', 'publisher', 'release_year', 'pages', 'photo')

	isbn = forms.CharField(
		max_length = 16,
		required = False,
		validators = [ RegexValidator(r'((978[\--– ])?[0-9][0-9\--– ]{10}[\--– ][0-9xX])|((978)?[0-9]{9}[0-9Xx])') ],
		widget = forms.TextInput(attrs = { 'placeholder': _('ISBN') })
	)
	publisher = forms.CharField(
		max_length = 64,
		required = False,
		widget = forms.TextInput(attrs = { 'placeholder': _('Publisher') })
	)
	pages = forms.IntegerField(
		min_value = 0,
		required = False,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Page count') })
	)
	release_year = forms.IntegerField(
		initial = strftime('%Y'),
		required = False,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Release year') })
	)
	language = forms.ChoiceField(
		required = False,
		choices = settings.LANGUAGES,
		widget = forms.Select(attrs = { 'placeholder': _('Language') })
	)
	description = forms.CharField(
		max_length = 4096,
		required = False,
		widget = forms.Textarea(attrs = { 'placeholder': _('Provide a brief description of this book...'), 'rows': '4' })
	)

class AddDigitalBookForm(forms.ModelForm):

	class Meta:
		model = DigitalBookCopy
		fields = ('pdf', 'isbn', 'language', 'description', 'publisher', 'release_year', 'pages', 'photo')

	pdf = forms.FileField(required = True)
	isbn = forms.CharField(
		max_length = 16,
		required = False,
		validators = [RegexValidator(r'((978[\--– ])?[0-9][0-9\--– ]{10}[\--– ][0-9xX])|((978)?[0-9]{9}[0-9Xx])')],
		widget = forms.TextInput(attrs = { 'placeholder': _('ISBN') })
	)
	publisher = forms.CharField(
		max_length = 64,
		required = False,
		widget = forms.TextInput(attrs = { 'placeholder': _('Publisher') })
	)
	pages = forms.IntegerField(
		min_value = 0,
	    initial = 0,
		required = False,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Page count') })
	)
	release_year = forms.IntegerField(
		initial = strftime('%Y'),
		required = False,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Release year') })
	)
	language = forms.ChoiceField(
		required = False,
		choices = settings.LANGUAGES,
		widget = forms.Select(attrs = { 'placeholder': _('Language') })
	)
	description = forms.CharField(
		max_length = 4096,
		required = False,
		widget = forms.Textarea(attrs = { 'placeholder': _('Provide a brief description of this book...'), 'rows': '4' })
	)

class AddPhysicalBookForm(forms.ModelForm):

	class Meta:
		model = PhysicalBookCopy
		fields = ('isbn', 'language', 'description', 'publisher', 'release_year', 'pages', 'photo')

	isbn = forms.CharField(
		max_length = 16,
		required = False,
		validators = [RegexValidator(r'((978[\--– ])?[0-9][0-9\--– ]{10}[\--– ][0-9xX])|((978)?[0-9]{9}[0-9Xx])')],
		widget = forms.TextInput(attrs = { 'placeholder': _('ISBN') })
	)
	publisher = forms.CharField(
		max_length = 64,
		required = False,
		widget = forms.TextInput(attrs = { 'placeholder': _('Publisher') })
	)
	pages = forms.IntegerField(
		min_value = 0,
	    initial = 0,
		required = False,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Page count') })
	)
	release_year = forms.IntegerField(
		initial = strftime('%Y'),
		required = False,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Release year') })
	)
	language = forms.ChoiceField(
		required = False,
		choices = settings.LANGUAGES,
		widget = forms.Select(attrs = { 'placeholder': _('Language') })
	)
	description = forms.CharField(
		max_length = 4096,
		required = False,
		widget = forms.Textarea(
			attrs = { 'placeholder': _('Provide a brief description of this book...'), 'rows': '4' })
	)
