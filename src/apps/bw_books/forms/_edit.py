# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from ..models import PhysicalBookCopy, DigitalBookCopy
from django.core.validators import RegexValidator
from imagekit.forms import ProcessedImageField
from django.forms import Form, ValidationError
from django.forms.models import model_to_dict
from pilkit.processors import SmartResize
from django.utils.timezone import now
from django.conf import settings
from imagekit import ImageSpec
from django import forms

__all__ = [ 'CopyEditForm' ]

class Thumbnail(ImageSpec):
	processors = [ SmartResize(100, 50) ]
	format = 'JPEG'
class CopyEditForm(Form):

	isbn = forms.CharField(
		required = False,
		validators = [ RegexValidator(r'((978[\--– ])?[0-9][0-9\--– ]{10}[\--– ][0-9xX])|((978)?[0-9]{9}[0-9Xx])') ],
		widget = forms.TextInput(attrs = { 'placeholder': _('ISBN') })
	)
	language = forms.CharField(
		required = False,
		widget = forms.Select(
			attrs = { 'placeholder': _('Language') },
			choices = settings.LANGUAGES
		)
	)
	description = forms.CharField(
		required = False,
		widget = forms.TextInput(attrs = { 'placeholder': _('Description') })
	)
	publisher = forms.CharField(
		required = False,
		widget = forms.TextInput(attrs = { 'placeholder': _('Publisher') })
	)
	photo = ProcessedImageField(
	    required = False,
	    spec_id = 'apps:bw_books:forms:_edit:Thumbnail'
	)
	release_year = forms.IntegerField(
		required = False,
		min_value = 0,
		max_value = now().year,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Release year') })
	)
	pages = forms.IntegerField(
		min_value = 0,
		required = False,
		widget = forms.NumberInput(attrs = { 'placeholder': _('Page No.') })
	)
	pdf = forms.FileField(
		allow_empty_file = False,
		required = False
	)

	def __init__(self, copy, data = None, files = None, **kwargs):

		self.copy = copy

		if data is None: data = model_to_dict(copy)
		super(CopyEditForm, self).__init__(data, files, model_to_dict(copy), **kwargs)

		if isinstance(copy, PhysicalBookCopy): self.type = 'physical'
		else: self.type = 'digital'

	@property
	def is_physical(self): return self.type == 'physical'
	@property
	def is_digital(self): return self.type == 'digital'

	def save(self, **kwargs):

		self.copy.isbn = self.cleaned_data.get('isbn', self.copy.isbn)
		self.copy.language = self.cleaned_data.get('language', self.copy.language)
		self.copy.description = self.cleaned_data.get('description', self.copy.description)
		self.copy.publisher = self.cleaned_data.get('publisher', self.copy.publisher)
		self.copy.release_year = int(self.cleaned_data.get('release_year', self.copy.release_year))
		self.copy.pages = int(self.cleaned_data.get('pages', self.copy.pages))

		photo = self.cleaned_data.get('photo', None)
		if photo is not None: self.copy.photo = photo

		self.copy.save(**kwargs)

		if self.is_digital:

			pdf = self.cleaned_data.get('pdf', None)
			if pdf is not None:

				self.copy.pdf = pdf
				self.copy.save()
		else:
			pass
