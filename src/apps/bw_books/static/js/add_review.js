
document.getElementById("add_review").onclick = function() {
    $.ajax({
        type: 'POST',
        url: "./add_review/", //- action form
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            review_content: $('#review_content').val(),
            book_id: $('#book_id').val()
        },
        success: function(data){
            document.getElementById('reviews').innerHTML = data;
            document.getElementById('add_review_form').style.display = 'None'
        }
    });
}

function add_recommendation() { // :(
    $.ajax({
        type: 'POST',
        url: "./recommend/", //- action form
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            book_id: $('#book_id').val()
        },
        success: function(data){
            document.getElementById('number_recommendations').innerHTML = data;
            document.getElementById('recommend').style.display = 'None'
        }
    });
}

function get_rating() { // :(
    $.ajax({
        type: 'GET',
        url: "./rate/", //- action form
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            book_id: $('#book_id').val()
        },
        success: function(data){
            if(data != -1) {
                for(var i = 1; i <= data; i++) {
                    document.getElementById('rating_'+i).innerHTML = '&#9733;';
                }
            }
        }
    });
}

function rate_book(rating) { // :(
    $.ajax({
        type: 'POST',
        url: "./rate/", //- action form
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            book_id: $('#book_id').val(),
            rating: rating
        },
        success: function(data){
            alert('Your rating has been updated.');
            document.getElementById('avg_rating').innerHTML = data;
            for(var i = 1; i <= rating; i++) {
                document.getElementById('rating_'+i).innerHTML = '&#9733;';
            }
            while(i <= 5){
                document.getElementById('rating_'+i).innerHTML = '&#9734;';
                i++;
            }
        }
    });
}