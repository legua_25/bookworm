
document.getElementById("add_genre").onclick = add_genre;
document.getElementById("genre").onkeydown = checkKey;

function checkKey(e) {
    if(e.which == 13) {
        add_genre();
    }
}

function add_genre() {
    $.ajax({
        type: 'POST',
        url: "./add_genre/", //- action form
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            genre: $('#genre').val(),
            book_id: $('#book_id').val()
        },
        success: function(data){
            document.getElementById('book_genres').innerHTML = data;
        }
    });
}