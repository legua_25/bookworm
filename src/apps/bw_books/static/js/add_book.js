
var wizard_step = 1;
var num_steps = 3;

function showWizardStep() {
    for (var i = 1; i <= num_steps; i++) {
        if(i == wizard_step) {
            document.getElementById("add_form_wizard_"+i).style.display = "block";
        } else {
            document.getElementById("add_form_wizard_" + i).style.display = "none";
        }
    }
}

function nextStep() {
    wizard_step++;
    if(wizard_step == num_steps) {
        document.getElementById("next").style.display = "none";
        document.getElementById("submit").style.display = "initial";
    } else {
        document.getElementById("next").style.display = "initial";
    }
    document.getElementById("back").style.display = "initial";
    showWizardStep();
}
function prevStep() {
    if(wizard_step == num_steps) {
        document.getElementById("submit").style.display = "none";
    }
    wizard_step--;
    if(wizard_step == 1) {
        document.getElementById("back").style.display = "none";
    } else {
        document.getElementById("back").style.display = "initial";
    }
    document.getElementById("next").style.display = "initial";
    showWizardStep();
}

window.onload = function(){
    document.getElementById("id_book_type").value = 'ph';
    showWizardStep();
    document.getElementById("back").style.display = "none";
    document.getElementById("physical_form").style.display = "block";
    document.getElementById("digital_form").style.display = "none";
    document.getElementById("id_price").style.display = "none";
    document.getElementById("price_label").style.display = "none";
    document.getElementById("submit").style.display = "none";
    document.getElementById("id_title").required = 'required';
};

document.getElementById("next").onclick = nextStep;
document.getElementById("back").onclick = prevStep;

document.getElementById("id_book_type").onchange = function() {
    var type = document.getElementById("id_book_type").value;
    if (type == 'ph') {
        document.getElementById("physical_form").style.display = "block";
        document.getElementById("digital_form").style.display = "none";
        document.getElementById("id_pdf").required = '';
    }
    else if (type == 'di') {
        document.getElementById("physical_form").style.display = "none";
        document.getElementById("digital_form").style.display = "block";
        document.getElementById("id_pdf").required = 'required';
    }
};

document.getElementById("id_for_sale").onclick = function() {
    if (document.getElementById("id_for_sale").checked) {
        document.getElementById("id_price").style.display = "initial";
        document.getElementById("price_label").style.display = "initial";
    }
    else {
        document.getElementById("id_price").style.display = "none";
        document.getElementById("price_label").style.display = "none";
    }
};

var nAuthors = 1;

document.getElementById("another_author").onclick = function() {
    var a_name = document.createElement("input");
    a_name.type = "text";
    a_name.id = "first_name_"+nAuthors;
    a_name.name = "first_name_"+nAuthors;
    a_name.maxlength = 64;

    var f_name = document.createElement("input");
    f_name.type = "text";
    f_name.id = "family_name_"+nAuthors;
    f_name.name = "family_name_"+nAuthors;
    f_name.maxlength = 64;

    document.getElementById("add_form_wizard_1").appendChild(a_name);
    document.getElementById("add_form_wizard_1").appendChild(f_name);

    nAuthors++;
};