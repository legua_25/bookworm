window.onload = function() {
    get_genres();
    get_reviews();
    get_description();
    if (typeof get_rating === "function") {
        get_rating();
    }
}

function get_genres() {
    $.ajax({
        type: 'GET',
        url: "./get_genres/", //- action form
        data: {
            book_id: $('#book_id').val()
        },
        success: function(data){
            document.getElementById('book_genres').innerHTML = data;
        }
    });
}

function get_reviews() {
    $.ajax({
        type: 'GET',
        url: "./get_reviews/", //- action form
        data: {
            book_id: $('#book_id').val()
        },
        success: function(data){
            document.getElementById('reviews').innerHTML = data;
        }
    });
}

function get_description() {
    return false;
}