$('#remove').click(function (e) {
    sure = confirm($('#book-meta').data('confirm'));
	if (sure) {
		$.ajax({
			type: 'GET',
			url: "./remove/",
            data: {
                copy_id: $('#copy_id').val()
            },
			success: function (data) {
				if (data) {
					window.location.replace("/accounts/");
				}
			}
		});
	}
});
