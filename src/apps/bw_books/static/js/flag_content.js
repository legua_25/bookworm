
function flag_book() {
    if (confirm($('#flag_confirm').data('confirm'))) {
        $.ajax({
            type: 'POST',
            url: "/books/flag/", //- action form
            data: {
                csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                book_id: $('#book_id').val()
            },
            success: function (data) {
                alert(data);
            }
        });
    }
}

function flag_copy() {
    if (confirm($('#flag_confirm').data('confirm'))) {
        $.ajax({
            type: 'POST',
            url: "/books/flag/", //- action form
            data: {
                csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                copy_id: $('#copy_id').val()
            },
            success: function (data) {
                alert(data);
            }
        });
    }
}