
window.onload = function() {
    $.ajax({
        type: 'GET',
        url: "./comments/",
        data: {
            copy_id: $('#copy_id').val()
        },
        success: function(data){
            document.getElementById('comments').innerHTML = data;
        }
    });
}

document.getElementById("add-comment").onclick = function() {
    $.ajax({
        type: 'POST',
        url: "./comment/add/",
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            comment_content: $('#comment_content').val(),
            copy_id: $('#copy_id').val()
        },
        success: function(data){
            document.getElementById('comments').innerHTML = data;
        }
    });
}