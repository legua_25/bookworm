# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig

class BookConfig(AppConfig):

	verbose_name = _('Book management')
	name = 'apps.bw_books'

default_app_config = 'apps.bw_books.BookConfig'
