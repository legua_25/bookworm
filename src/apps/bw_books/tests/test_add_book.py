# -*- coding: utf-8 -*-
from unittest import TestCase
from django.test import Client
from ..models import Book, Author, PhysicalBookCopy, DigitalBookCopy
from ..forms.book_forms import AddBookForm, AddAuthorForm, AddBookCopyForm, AddPhysicalBookForm, AddDigitalBookForm
from apps.bw_user.models import User

class AddBookTest(TestCase):
    def setUp(self):
        self.client = Client()

        #author = Author.objects.create(first_name = 'Nombre', family_name = 'Apellido')
        #book = Book.objects.create(title = 'Título')
        #book.authors.add(author)
        #book.save()

    def test_add_book_view(self):
        response = self.client.get('/books/add/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)