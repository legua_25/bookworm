# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.bw_books.models._book
import imagekit.models.fields
import django.core.validators
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='author UUID', db_index=True)),
                ('first_name', models.CharField(default='', max_length=64, verbose_name='first name', blank=True)),
                ('family_name', models.CharField(default='', max_length=64, verbose_name='family name', blank=True)),
                ('biography', models.TextField(default='', verbose_name='brief biography')),
                ('addition_date', models.DateTimeField(auto_now_add=True, verbose_name='addition date')),
                ('flags', models.PositiveIntegerField(default=0, verbose_name='flags')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'authors',
                'verbose_name': 'author',
                'verbose_name_plural': 'authors',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='book UUID', db_index=True)),
                ('title', models.CharField(default='', max_length=64, verbose_name='book title')),
                ('description', models.TextField(default='', verbose_name='brief description')),
                ('addition_date', models.DateTimeField(auto_now_add=True, verbose_name='addition date')),
                ('flags', models.PositiveIntegerField(default=0, verbose_name='flags')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'books',
                'verbose_name': 'book',
                'verbose_name_plural': 'books',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BookCopy',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='book copy UUID', db_index=True)),
                ('isbn', models.CharField(default='', max_length=18, verbose_name='ISBN', blank=True)),
                ('photo', imagekit.models.fields.ProcessedImageField(default=None, upload_to=apps.bw_books.models._book._get_upload_site, null=True, verbose_name='book photo', blank=True)),
                ('language', models.CharField(default='en-us', max_length=10, verbose_name='language', blank=True, choices=[('en', 'English'), ('es', 'Spanish')])),
                ('description', models.TextField(default='', verbose_name='brief description', blank=True)),
                ('publisher', models.CharField(default='', max_length=64, verbose_name='publisher', blank=True)),
                ('release_year', models.IntegerField(default=0, null=True, verbose_name='release year', blank=True)),
                ('pages', models.PositiveIntegerField(default=0, verbose_name='number of pages', blank=True)),
                ('addition_date', models.DateTimeField(auto_now_add=True, verbose_name='addition date')),
                ('flags', models.PositiveIntegerField(default=0, verbose_name='flags')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'book_copies',
                'verbose_name': 'book copy',
                'verbose_name_plural': 'book copies',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='comment UUID', db_index=True)),
                ('content', models.CharField(default='', max_length=1024, verbose_name='content')),
                ('addition_date', models.DateTimeField(auto_now_add=True, verbose_name='addition date')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'comments',
                'verbose_name': 'book comment',
                'verbose_name_plural': 'book comments',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DigitalBookCopy',
            fields=[
                ('bookcopy_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='bw_books.BookCopy')),
                ('pdf', models.FileField(upload_to=apps.bw_books.models._book._get_upload_site, max_length=512, verbose_name='digital book pdf')),
            ],
            options={
                'db_table': 'digital_books',
                'verbose_name': 'digital book',
                'verbose_name_plural': 'digital books',
            },
            bases=('bw_books.bookcopy',),
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='genre UUID', db_index=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name='name')),
                ('addition_date', models.DateTimeField(auto_now_add=True, verbose_name='addition date')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'genre',
                'verbose_name': 'genre',
                'verbose_name_plural': 'genres',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GenreTag',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='genre tag UUID', db_index=True)),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'genre_tag',
                'verbose_name': 'genre tag',
                'verbose_name_plural': 'genre tags',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PhysicalBookCopy',
            fields=[
                ('bookcopy_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='bw_books.BookCopy')),
            ],
            options={
                'db_table': 'physical_books',
                'verbose_name': 'physical book',
                'verbose_name_plural': 'physical books',
            },
            bases=('bw_books.bookcopy',),
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='comment UUID', db_index=True)),
                ('rating', models.PositiveIntegerField(default=0, verbose_name='rating', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(5)])),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'ratings',
                'verbose_name': 'rating',
                'verbose_name_plural': 'ratings',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Recommendation',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='comment UUID', db_index=True)),
                ('active', models.BooleanField(default=True, verbose_name='active')),
            ],
            options={
                'db_table': 'recommend',
                'verbose_name': 'recommendation',
                'verbose_name_plural': 'recommendations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('uid', django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, name=b'uid', blank=True, verbose_name='comment UUID', db_index=True)),
                ('content', models.CharField(default='', max_length=4096, verbose_name='content')),
                ('addition_date', models.DateTimeField(auto_now_add=True, verbose_name='addition date')),
                ('approved', models.BooleanField(default=False, verbose_name='approved')),
                ('flags', models.PositiveIntegerField(default=0, verbose_name='flags')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
                ('book', models.ForeignKey(related_name='reviews', verbose_name='book', to='bw_books.Book')),
            ],
            options={
                'db_table': 'reviews',
                'verbose_name': 'review',
                'verbose_name_plural': 'reviews',
            },
            bases=(models.Model,),
        ),
    ]
