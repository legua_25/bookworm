from __future__ import unicode_literals
from django.conf.urls import patterns, url, include

urlpatterns = patterns('apps.bw_books.views',

	url(r'^(?P<book_id>[a-f0-9]{32})/', include(patterns('apps.bw_books.views',

	    url(r'^$', 'book_info_views.book_info', name = 'book_info'),
        url(r'^get_genres/$', 'genre_views.get_genres', name = 'get_genres'),
        url(r'^add_genre/$', 'genre_views.add_genre', name = 'add_genre'),
        url(r'^get_reviews/$', 'book_rel_views.get_reviews', name = 'get_reviews'),
        url(r'^add_review/$', 'book_rel_views.add_review', name = 'add_review'),
        url(r'^recommend/$', 'book_rel_views.recommend', name = 'recommend'),
        url(r'^rate/$', 'book_rel_views.rate_book', name = 'rate_book'),

        url(r'^copy/(?P<book_copy_id>[a-f0-9]{32})/', include(patterns('apps.bw_books.views',
            url(r'^$', 'book_info_views.details', name = 'details'),
            url(r'^read/$', 'book_info_views.read', name = 'read'),
            url(r'^comments/$', 'book_rel_views.get_comments', name = 'get_comments'),
            url(r'^comment/add/$', 'book_rel_views.add_comment', name = 'add_comment'),
            url(r'^edit/$', 'book_rel_views.edit_book', name = 'edit_book'),
            url(r'^remove/$', 'book_rel_views.remove_book', name = 'book-copy-remove'),
        ))),

	))),

    url(r'^', include(patterns('apps.bw_books.views',

        url(r'^$', 'book_list_views.list_books', name = 'list-books'),
        url(r'^authors/(?P<author_id>[0-9a-f]{32})/', 'book_info_views.author_info', name ='author_info'),
        url(r'^genres/(?P<genre_id>[0-9a-f]{32})/', 'genre_views.genre_info', name ='genre_info'),
        url(r'^add/$', 'add_book_view.add_book', name = 'add-book'),
        url(r'^flag/$', 'book_rel_views.flag', name = 'flag'),

    ))),

)
