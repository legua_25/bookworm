# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import ModelAdmin, register
from .models import Book, Author, PhysicalBookCopy, DigitalBookCopy, Genre, Review, Comment

@register(Book)
class BookAdmin(ModelAdmin):

	date_hierarchy = 'addition_date'
	fieldsets = (
		(_('General'), { 'fields': [ 'uid', 'title', 'authors', 'description' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'flags', 'active' ] })
	)
	readonly_fields = [ 'uid' ]
	search_fields = [ 'uid', 'title' ]

	def save_model(self, request, obj, form, change):
		obj.save()

@register(Author)
class AuthorAdmin(ModelAdmin):

	date_hierarchy = 'addition_date'
	fieldsets = (
		(_('General'), { 'fields': [ 'uid', 'first_name', 'family_name', 'biography' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'flags', 'active' ] })
	)
	readonly_fields = [ 'uid' ]
	search_fields = [ 'uid', 'first_name', 'family_name' ]

	def save_model(self, request, obj, form, change):
		obj.save()

@register(PhysicalBookCopy)
class PhysicalBookCopyAdmin(ModelAdmin):

	date_hierarchy = 'addition_date'
	fieldsets = (
		(_('General'), { 'fields': [ 'uid', 'isbn', 'book', 'owner', 'photo', 'language', 'description', 'publisher' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'flags', 'active' ] })
	)
	readonly_fields = [ 'uid' ]
	search_fields = [ 'uid', 'isbn', 'book__title', 'owner__username' ]

	def save_model(self, request, obj, form, change):
		obj.save()

@register(DigitalBookCopy)
class DigitalBookCopyAdmin(ModelAdmin):

	date_hierarchy = 'addition_date'
	fieldsets = (
		(_('General'), { 'fields': [ 'uid', 'isbn', 'book', 'owner', 'photo', 'language', 'description', 'publisher', 'pdf' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'flags', 'active' ] })
	)
	readonly_fields = [ 'uid' ]
	search_fields = [ 'uid', 'isbn', 'book', 'owner' ]

	def save_model(self, request, obj, form, change):
		obj.save()

@register(Genre)
class GenreAdmin(ModelAdmin):

	date_hierarchy = 'addition_date'
	fieldsets = (
		(_('General'), { 'fields': [ 'uid', 'name' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'active' ] })
	)
	readonly_fields = [ 'uid' ]
	search_fields = [ 'uid', 'name' ]

	def save_model(self, request, obj, form, change):
		obj.save()

@register(Comment)
class CommentAdmin(ModelAdmin):

	date_hierarchy = 'addition_date'
	fieldsets = (
		(_('General'), { 'fields': [ 'uid', 'book', 'commenter', 'content' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'active' ] })
	)
	readonly_fields = [ 'uid' ]
	search_fields = [ 'uid', 'book__book__title', 'commenter__username', 'content' ]

	def save_model(self, request, obj, form, change):
		obj.save()

@register(Review)
class ReviewAdmin(ModelAdmin):

	date_hierarchy = 'addition_date'
	fieldsets = (
		(_('General'), { 'fields': [ 'uid', 'book', 'reviewer', 'content', 'approved' ] }),
		(_('Advanced'), { 'classes': [ 'collapse' ], 'fields': [ 'flags', 'active' ] })
	)
	readonly_fields = [ 'uid' ]
	search_fields = [ 'uid', 'book__title', 'reviewer__username', 'content', 'approved' ]

	def save_model(self, request, obj, form, change):
		obj.save()