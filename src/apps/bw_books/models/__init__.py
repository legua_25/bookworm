# -*- coding: utf-8 -*-
from _author import Author
from _book import Book, DigitalBookCopy, PhysicalBookCopy, BookCopy
from _book_rel import Comment, Recommendation, Rating, Review, Genre, GenreTag
