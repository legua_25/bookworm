# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django_extensions.db.fields import UUIDField
from django.utils.translation import ugettext_lazy as _
from django.db.models import Model, Manager
from django.db import models
from uuid import UUID
import wikipedia

class AuthorManager(Manager):
    def active(self): return self.get_queryset().filter(active = True)
class Author(Model):

    class Meta(object):
        unique_together = [ 'first_name', 'family_name' ]
        verbose_name = _('author')
        verbose_name_plural = _('authors')
        app_label = 'bw_books'
        db_table = 'authors'

    uid = UUIDField(
        primary_key = True,
        db_index = True,
        auto = True,
        editable = False,
        verbose_name = _('author UUID')
    )

    first_name = models.CharField(
        max_length = 64,
        default = '',
        blank = True,
        null = False,
        verbose_name = _('first name')
    )
    family_name = models.CharField(
        max_length = 64,
        default = '',
        blank = True,
        verbose_name = _('family name')
    )
    biography = models.TextField(
        default = '',
        verbose_name =  _('brief biography') #provided by crawler
    )
    addition_date = models.DateTimeField(
        auto_now_add = True,
        auto_now = False,
        editable = False,
        verbose_name = _('addition date')
    )
    flags = models.PositiveIntegerField(
        default = 0,
        verbose_name = _('flags')
    )
    active = models.BooleanField(
        default = True,
        verbose_name = _('active')
    )

    objects = AuthorManager()

    def update_biography(self):
        if self.first_name != 'Anonymous':
            page = wikipedia.search(self.first_name + ' ' + self.family_name)[0]
            if bool(page) is False: return False
            try:
                self.biography = wikipedia.summary(page)
            except:
                self.biography = ''
                return False
        else:
            self.biography = wikipedia.summary('anonymous work')
        return True

    @property
    def uid_str(self): return UUID(self.uid).hex

    def __str__(self): return '%s %s' % (self.first_name, self.family_name)
