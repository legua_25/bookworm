# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db.models import Model, Manager, Avg
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from uuid import UUID

class CommentManager(Manager):

	def active(self): return self.get_queryset().filter(active = True)
class Comment(Model):

	class Meta(object):
		verbose_name = _('book comment')
		verbose_name_plural = _('book comments')
		app_label = 'bw_books'
		db_table = 'comments'

	uid = UUIDField(
		primary_key = True,
		db_index = True,
		auto = True,
		editable = False,
		verbose_name = _('comment UUID')
	)
	book = models.ForeignKey(
		'bw_books.BookCopy',
		related_name = 'comments',
		verbose_name = _('book copy')
	)
	commenter = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		related_name = 'comments',
		verbose_name = _('commenter')
	)
	content = models.CharField(
		max_length = 1024,
		default = '',
		null = False,
		verbose_name = _('content')
	)
	addition_date = models.DateTimeField(
		auto_now_add = True,
		auto_now = False,
		verbose_name = _('addition date')
	)
	active = models.BooleanField(
		default = True,
		verbose_name = _('active')
	)

	objects = CommentManager()

	@property
	def uid_str(self): return UUID(self.uid).hex

	def __str__(self): return "%s's for %s" % (self.commenter.username, self.book)

class ReviewManager(Manager):

	def active(self): return self.get_queryset().filter(active = True)
class Review(Model):

	class Meta(object):
		unique_together = [ 'reviewer', 'book' ]
		verbose_name = _('review')
		verbose_name_plural = _('reviews')
		app_label = 'bw_books'
		db_table = 'reviews'

	uid = UUIDField(
		primary_key = True,
		db_index = True,
		auto = True,
		editable = False,
		verbose_name = _('comment UUID')
	)
	book = models.ForeignKey(
		'bw_books.Book',
		related_name = 'reviews',
		verbose_name = _('book')
	)
	reviewer = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		related_name = 'reviews',
		verbose_name = _('reviewer')
	)
	content = models.CharField(
		max_length = 4096,
		default = '',
		null = False,
		verbose_name = _('content')
	)
	addition_date = models.DateTimeField(
		auto_now_add = True,
		auto_now = False,
		verbose_name = _('addition date')
	)
	approved = models.BooleanField(
		default = False,
		verbose_name = _('approved')
	)
	flags = models.PositiveIntegerField(
		default = 0,
		verbose_name = _('flags')
	)
	active = models.BooleanField(
		default = True,
		verbose_name = _('active')
	)

	objects = ReviewManager()

	@property
	def uid_str(self): return UUID(self.uid).hex

	def __str__(self): return "%s's for %s (%sapproved)" % (self.reviewer.username, self.book, 'NOT ' if not self.approved else '')

class RatingManager(Manager):

	def active(self): return self.get_queryset().filter(active = True)
	def average_for(self, book): return self.get_queryset().filter(active = True, book = book).aggregate(Avg('rating')).values()[0] or 0
class Rating(Model):

	class Meta(object):
		verbose_name = _('rating')
		verbose_name_plural = _('ratings')
		app_label = 'bw_books'
		db_table = 'ratings'

	uid = UUIDField(
		primary_key = True,
		db_index = True,
		auto = True,
		editable = False,
		verbose_name = _('comment UUID')
	)
	book = models.ForeignKey(
		'bw_books.Book',
		related_name = 'ratings',
		verbose_name = _('book')
	)
	rater = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		related_name = 'ratings',
		verbose_name = _('rater')
	)
	rating = models.PositiveIntegerField(
		default = 0,
		validators = [ MinValueValidator(0), MaxValueValidator(5) ],
		verbose_name = _('rating')
	)
	active = models.BooleanField(
		default = True,
		verbose_name = _('active')
	)

	objects = RatingManager()

	@property
	def uid_str(self): return UUID(self.uid).hex

class RecommendationManager(Manager):

	def active(self): return self.get_queryset().filter(active = True)
	def for_book(self, book): return self.get_queryset().filter(active = True, book = book)
	def recommend_ratio(self, book):

		recommends = self.for_book(book)

		titles = []

		#for author in book.authors:
			#for title in author.titles:
				#titles.append(title)

		total = own = len(recommends)
		for title in titles:
			total += len(Recommendation.objects.for_book(title))

		if total == 0: return (Rating.objects.average_for(book) / 2)

		authors = float(own / total)
		return ((authors + Rating.objects.average_for(book)) / 2)
class Recommendation(Model):

	class Meta(object):
		verbose_name = _('recommendation')
		verbose_name_plural = _('recommendations')
		app_label = 'bw_books'
		db_table = 'recommend'

	uid = UUIDField(
		primary_key = True,
		db_index = True,
		auto = True,
		editable = False,
		verbose_name = _('comment UUID')
	)
	book = models.ForeignKey(
		'bw_books.Book',
		related_name = '+',
		verbose_name = _('book')
	)
	recommender = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		related_name = 'recommendations',
		verbose_name = _('recommender')
	)
	active = models.BooleanField(
		default = True,
		verbose_name = _('active')
	)

	objects = RecommendationManager()

	@property
	def uid_str(self): return UUID(self.uid).hex

class GenreManager(Manager):

	def active(self): return self.get_queryset().filter(active = True)
	def for_book_copy(self, book_copy):

		book = book_copy.book
		return self.get_queryset().filter(active = True, tags__book = book)
class Genre(Model):

	class Meta(object):
		verbose_name = _('genre')
		verbose_name_plural = _('genres')
		app_label = 'bw_books'
		db_table = 'genre'

	uid = UUIDField(
		primary_key = True,
		db_index = True,
		auto = True,
		editable = False,
		verbose_name = _('genre UUID')
	)
	name = models.CharField(
		null = False,
		unique = True,
		max_length = 255,
		verbose_name = _('name')
	)
	addition_date = models.DateTimeField(
		auto_now_add = True,
		auto_now = False,
		editable = False,
		verbose_name = _('addition date')
	)
	active = models.BooleanField(
		default = True,
		verbose_name = _('active')
	)

	objects = GenreManager()

	@property
	def uid_str(self): return UUID(self.uid).hex

	def __hash__(self): return hash(UUID(self.uid))

	def __str__(self): return self.name

class GenreTagManager(Manager):
	def active(self): return self.get_queryset().filter(active = True)
class GenreTag(Model):

	class Meta(object):
		verbose_name = _('genre tag')
		verbose_name_plural = _('genre tags')
		app_label = 'bw_books'
		db_table = 'genre_tag'

	uid = UUIDField(
		primary_key = True,
		db_index = True,
		auto = True,
		editable = False,
		verbose_name = _('genre tag UUID')
	)
	book = models.ForeignKey(
		'bw_books.Book',
		related_name = 'tags',
		verbose_name = _('book')
	)
	genre = models.ForeignKey(
		Genre,
		related_name = 'tags',
		verbose_name = _('genre')
	)
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		related_name = 'tags',
		verbose_name = _('user')
	)
	active = models.BooleanField(
		default = True,
		verbose_name = _('active')
	)

	objects = GenreTagManager()

	@property
	def uid_str(self): return UUID(self.uid).hex
