# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django_extensions.db.fields import UUIDField
from django.utils.translation import ugettext_lazy as _, ugettext_noop
from django.conf import settings
from django.db.models import Model, Manager
from django.db import models
from imagekit.models import ProcessedImageField
from pilkit.processors import SmartResize
from model_utils.managers import InheritanceManager
from uuid import UUID
from apps.bw_trans.models import Giveaway, Purchase, Trade
from ._book_rel import Genre, GenreTag, Recommendation
import wikipedia

Transaction = 'Transaction'

def _get_upload_site(instance, filename):

    return './users/%s/books/%s' % (
        instance.owner.uid_str,
        filename
    )

class BookManager(Manager):
    def active(self): return self.get_queryset().filter(active = True)
class Book(Model):

    class Meta(object):
        verbose_name = _('book')
        verbose_name_plural = _('books')
        app_label = 'bw_books'
        db_table = 'books'

    uid = UUIDField(
        primary_key = True,
        db_index = True,
        auto = True,
        editable = False,
        verbose_name = _('book UUID')
    )
    title = models.CharField(
        max_length = 64,
        default = '',
        blank = False,
        verbose_name = _('book title')
    )
    authors = models.ManyToManyField(
        'bw_books.Author',
        related_name = 'titles',
        verbose_name = _('book authors')
    )
    description = models.TextField(
        default = '',
        verbose_name = _('brief description')  # provided by crawler
    )
    addition_date = models.DateTimeField(
        auto_now_add = True,
        auto_now = False,
        editable = False,
        verbose_name = _('addition date')
    )
    flags = models.PositiveIntegerField(
        default = 0,
        verbose_name = _('flags')
    )
    active = models.BooleanField(
        default = True,
        verbose_name = _('active')
    )

    objects = BookManager()

    def update_description(self):

        page = wikipedia.search(self.title)
        if bool(page) is False:
            page = wikipedia.search(self.title + ' (novel)')
            if bool(page) is False: return False

        try:
            self.description = wikipedia.summary(page[0], auto_suggest = False)
        except:
            self.description = ''
            return False
        return True

    @property
    def number_recommendations(self): return len(Recommendation.objects.filter(book = self))
    @property
    def number_ratings(self): return self.ratings.all().count()
    @property
    def average_rating(self): return self.ratings.aggregate(models.Avg('rating')).values()[0]
    @property
    def uid_str(self): return UUID(self.uid).hex

    def __str__(self): return self.title

class BookCopyManager(InheritanceManager):

    def active(self): return self.get_queryset().filter(active = True)
    def get_recommended_for(self, user):

        books = self.active().filter(owner = user)
        titles_owned = []
        if books.exists():

            # Common genres for user's books.
            genres = []
            for book in books:
                for g in GenreTag.objects.active().filter(book = book.book):
                    if g.genre not in genres: genres.append(g.genre)
                if book.book not in titles_owned: titles_owned.append(book.book)

            if bool(genres) is True:

                # Books with user's genres.
                recommended = []
                for genre in genres:
                    for tag in GenreTag.objects.active().filter(genre = genre):
                        if tag.book not in recommended and tag.book not in titles_owned:
                            recommended.append(tag.book)

                def recommend_sort(b1, b2):

                    rb1 = Recommendation.objects.recommend_ratio(b1)
                    rb2 = Recommendation.objects.recommend_ratio(b2)

                    return int(rb2 - rb1)
                recommended.sort(cmp = recommend_sort)

                # Available copies of recommended books.
                recommended_copies = []
                for book in recommended:
                    for copy in BookCopy.objects.active().filter(book = book):
                        recommended_copies.append(copy)

                return recommended_copies[:25]

        return [ book for book in BookCopy.objects.active().exclude(owner = user).order_by('-addition_date')[:25] ]
class BookCopy(Model):

    class Meta(object):
        verbose_name = _('book copy')
        verbose_name_plural = _('book copies')
        app_label = 'bw_books'
        db_table = 'book_copies'

    uid = UUIDField(
        primary_key = True,
        db_index = True,
        auto = True,
        editable = False,
        verbose_name = _('book copy UUID')
    )
    isbn = models.CharField(
        max_length = 18,
        default = '',
        blank = True,
        verbose_name = _('ISBN')
    )
    book = models.ForeignKey(
        Book,
        related_name = 'book_copies',
        verbose_name = _('book source')
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        default = None,
        related_name = 'books',
        verbose_name = _('owner')
    )
    photo = ProcessedImageField(
        processors = [ SmartResize(140, 180) ],
        default = None,
        blank = True,
        null = True,
        format = 'JPEG',
        upload_to = _get_upload_site,
        verbose_name = _('book photo')
    )
    language = models.CharField(
        blank = True,
        default = settings.LANGUAGE_CODE,
        choices = settings.LANGUAGES,
        max_length = 10,
        verbose_name = _('language')
    )
    description = models.TextField(
        default = '',
        blank = True,
        verbose_name = _('brief description')  # provided by user
    )
    publisher = models.CharField(
        max_length = 64,
        blank = True,
        default = '',
        verbose_name = _('publisher')
    )
    release_year = models.IntegerField(
        null = True,
        blank = True,
        default = 0,
        verbose_name = _('release year')
    )
    pages = models.PositiveIntegerField(
        blank = True,
        default = 0,
        verbose_name = _('number of pages')
    )
    addition_date = models.DateTimeField(
        auto_now_add = True,
        auto_now = False,
        editable = False,
        verbose_name = _('addition date')
    )
    flags = models.PositiveIntegerField(
        default = 0,
        verbose_name = _('flags')
    )
    active = models.BooleanField(
        default = True,
        verbose_name = _('active')
    )

    objects = BookCopyManager()

    @property
    def uid_str(self): return UUID(self.uid).hex

    def __str__(self): return "%s's %s" % (self.owner.username, self.book)

class DigitalBookCopy(BookCopy):

    class Meta(object):

        verbose_name = _('digital book')
        verbose_name_plural = _('digital books')
        app_label = 'bw_books'
        db_table = 'digital_books'

    pdf = models.FileField(
        blank = False,
        max_length = 512,
        upload_to = _get_upload_site,
        verbose_name = _('digital book pdf')
    )
class PhysicalBookCopy(BookCopy):

    class Meta(object):

        verbose_name = _('physical book')
        verbose_name_plural = _('physical books')
        app_label = 'bw_books'
        db_table = 'physical_books'

    @property
    def for_trade(self):

        transacts = filter(lambda t: isinstance(t, Trade), self.transacts.select_subclasses(Trade))
        return bool(transacts) is True
    @property
    def for_sale(self):

        transacts = filter(lambda t: isinstance(t, Purchase), self.transacts.select_subclasses(Purchase))
        return bool(transacts) is True
    @property
    def for_giveaway(self):

        transacts = filter(lambda t: isinstance(t, Giveaway), self.transacts.select_subclasses(Giveaway))
        return bool(transacts) is True

    @property
    def trade_id(self):
        transact = filter(lambda t: isinstance(t, Trade), self.transacts.select_subclasses(Trade))
        if bool(transact) is True: return transact[0].uid_str
        return ''
    @property
    def sale_id(self):
        transact = filter(lambda t: isinstance(t, Purchase), self.transacts.select_subclasses(Purchase))
        if bool(transact) is True: return transact[0].uid_str
        return ''
    @property
    def giveaway_id(self):
        transact = filter(lambda t: isinstance(t, Giveaway), self.transacts.select_subclasses(Giveaway))
        if bool(transact) is True: return transact[0].uid_str
        return ''